<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PassedQuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Passed Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= (!Yii::$app->user->can('createPassedQuestion')) ? '' :
            Html::a('Create Passed Question', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'passed_exam_id',
            'question_id',
            'point',
            'status',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('viewPassedQuestion'),
                    'update' => Yii::$app->user->can('updatePassedQuestion'),
                    'delete' => Yii::$app->user->can('deletePassedQuestion'),
                ],
            ],
        ],
    ]); ?>

</div>
