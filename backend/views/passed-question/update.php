<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PassedQuestion */

$this->title = 'Update Passed Question: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Passed Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="passed-question-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
