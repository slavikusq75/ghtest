<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PassedQuestion */

$this->title = 'Create Passed Question';
$this->params['breadcrumbs'][] = ['label' => 'Passed Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-question-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
