<?php

use common\models\Answer;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $questionsList array */

$this->title = 'Answers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= (!Yii::$app->user->can('createAnswer')) ? '' :
            Html::a('Create Answer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'question_id' => [
                'attribute' => 'question_id',
                'label' => 'Exam title',
                //'filter' => $questionsList,
                'value' => function (Answer $model) {
                    return $model->question->title;
                }
            ],
            'description' => [
                'filter' => $questionsList,
                'attribute' => 'question_id',
                'label' => 'Question description',
                'value' => 'question.description'],
            'right',
            'description',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('viewAnswer'),
                    'update' => Yii::$app->user->can('updateAnswer'),
                    'delete' => Yii::$app->user->can('deleteAnswer'),
                ],
            ],
        ],
    ]); ?>

</div>
