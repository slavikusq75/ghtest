<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Answer */
/* @var $questionsList array */
/* @var $questionsDescriptionList array */

$this->title = 'Create Answer';
$this->params['breadcrumbs'][] = ['label' => 'Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="answer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'questionsList' => $questionsList,
        'questionsDescriptionList' => $questionsDescriptionList,
    ]) ?>

</div>
