<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Answer */
/* @var $form yii\widgets\ActiveForm */
/* @var $questionsList array */
/* @var $questionsDescriptionList array */
?>

<div class="answer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question_id')->label('Question title')->dropDownList($questionsList) ?>

    <?= $form->field($model, 'question_id')->label('Question description')->dropDownList($questionsDescriptionList) ?>

    <?= $form->field($model, 'description')->label('Answer description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'right')->radioList([0 => 'No', 1 => 'Yes']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
