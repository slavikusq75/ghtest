<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PassedExam */

$this->title = 'Update Passed Exam: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Passed Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="passed-exam-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
