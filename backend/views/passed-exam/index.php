<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\PassedExam;
use common\components\widgets\PercentCalcWidget;

/* @var $this yii\web\View */
/* @var $passedExams array */
/* @var $passedExam common\models\PassedExamSearch */
/* @var $resultDataProvider yii\data\ActiveDataProvider */
/* @var $coursesList array */
/* @var $examsList array */
/* @var $searchModel common\models\ExamSearch */

$this->title = 'User results';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-exam-index">

    <h1><?= Html::encode($this->title) ?></h1>

</div>

<div class="passedexam-index">

    <?= GridView::widget([
        'dataProvider' => $resultDataProvider,
        'filterModel' => $searchModel,
        'perfectScrollbar' => true,
        'floatHeader' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Passed exams</h3>',
            'type' => 'success',
            'footer' => false
        ],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id' => [
                'attribute' => 'id',
                'label' => 'PassedExam id',
                'filter' => false,
                'value' => function (PassedExam $model) {
                    return $model->id;
                }
            ],
            'user.lastname' => [
                'attribute' => 'lastname',
                'label' => 'Last name',
                'value' => 'user.lastname',
            ],
            'user.firstname' => [
                'attribute' => 'user.firstname',
                'label' => 'First name',
                'filter' => $coursesList,
                'value' => 'user.firstname',
            ],
            'season.title' => [
                'attribute' => 'season.title',
                'label' => 'Season title',
                'filter' => $examsList,
                'value' => function (PassedExam $model) {
                    return $model->exam->course->season->title;
                }
            ],
            'course.description' => [
                'attribute' => 'course.description',
                'label' => 'Course title',
                'filter' => $examsList,
                'value' => function (PassedExam $model) {
                    return $model->exam->course->title;
                }
            ],
            'exam.title' => [
                'attribute' => 'id',
                'label' => 'Exam title',
                'filter' => $examsList,
                'value' => function (PassedExam $model) {
                    return $model->exam->title;
                }
            ],
            'passed_exam.point' => [
                'attribute' => 'point',
                'label' => 'Total points',
                'filter' => $examsList,
                'value' => function (PassedExam $model) {
                    return $model->point;
                }
            ],
            'correct_percents' => [
                'attribute' => 'exam.questions',
                'label' => 'Correct percents',
                'filter' => $coursesList,
                'value' => function ($data) {
                    return PercentCalcWidget::widget(['percentcalc' => $data]);
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>