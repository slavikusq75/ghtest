<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use common\models\Question;
use common\components\widgets\MarkPassedAnswersWidget;
use common\components\widgets\PassedQuestionPointWidget;

/* @var $this yii\web\View */
/* @var $model common\models\PassedExam */
/* @var $passedQuestion common\models\PassedQuestion */
/* @var $questionDataProvider yii\data\ActiveDataProvider */
/* @var $questionSearch common\models\QuestionSearch */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Passed Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-exam-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= (!Yii::$app->user->can('createPassedExam')) ? '' :
            Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= (!Yii::$app->user->can('deletePassedAnswer')) ? '' :
            Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'user.lastname',
            'user.firstname',
            [
                'label' => 'Exam title',
                'value' => $model->exam->title,
            ],
            [
                'label' => 'Exam description',
                'value' => $model->exam->description,
            ],
            'timer',
            'status',
            [
                'label' => 'Total points',
                'value' => $model->point,
            ],
        ],
    ]) ?>

</div>

<div class="passedexam-index">

    <?= GridView::widget([
        'dataProvider' => $questionDataProvider,
        'filterModel' => $questionSearch,
        'floatHeader' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Exam results</h3>',
            'type' => 'success',
            'footer' => false
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'description',
            'point',
            'passedPoint' => [
                'attribute' => 'passedPoint',
                'label' => 'Passed Point',
                'value' => function (Question $model) {
                    return PassedQuestionPointWidget::widget(['question' => $model]);
                },
            ],
            'answers' => [
                'attribute' => 'answer.description',
                'label' => 'Answers list',
                'format' => 'html',
                'value' => function (Question $model) {
                    return MarkPassedAnswersWidget::widget(['question' => $model]);
                },
            ],
        ],
    ]); ?>

</div>
