<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PassedExam */

$this->title = 'Create Passed Exam';
$this->params['breadcrumbs'][] = ['label' => 'Passed Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-exam-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
