<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 06.04.16
 * Time: 21:54
 */

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\InviteTeacherForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Invite teacher';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-invite-teacher">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out teacher's email. A link to register a teacher account will be sent there.</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'invite-teacher']); ?>

            <?= $form->field($model, 'email') ?>

            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
