<?php

use common\models\Question;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\QuestionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $examsList array */

$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="question-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= (!Yii::$app->user->can('createQuestion')) ? '' :
            Html::a('Create Question', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'exam_id' => [
                'attribute' => 'exam_id',
                'label' => 'Exam',
                'filter' => $examsList,
                'value' => function (Question $model) {
                    return $model->exam->title;
                }
            ],
            'title',
            'description',
            'type',
            'point',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('viewQuestion'),
                    'update' => Yii::$app->user->can('updateQuestion'),
                    'delete' => Yii::$app->user->can('deleteQuestion'),
                ],
            ],
        ],
    ]); ?>

</div>
