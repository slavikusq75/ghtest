<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Question */
/* @var $answerDataProvider yii\data\ActiveDataProvider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$titleAnswers = 'Answers';

?>
<div class="question-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= (!Yii::$app->user->can('createAnswer')) ? '' :
            Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= (!Yii::$app->user->can('deleteAnswer')) ? '' :
            Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'exam_id' => [
                'label' => 'Exam',
                'value' => $model->exam->title,
            ],
            'title',
            'description',
            'type',
            'point',
        ],
    ]) ?>

    <div class="answer-index">

        <h1><?= Html::encode($titleAnswers) ?></h1>

        <?= GridView::widget([
            'dataProvider' => $answerDataProvider,
            'columns' => [
                'id',
                'question_id',
                'description',
                'right',

                ['class' => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'view' => Yii::$app->user->can('viewAnswer'),
                        'update' => Yii::$app->user->can('updateAnswer'),
                        'delete' => Yii::$app->user->can('deleteAnswer'),
                    ],
                ],
            ],
        ]);
        ?>

    </div>


</div>
