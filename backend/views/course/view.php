<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $seasonsList array */
/* @var $exams array */
/* @var $newExam common\models\Exam */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary update-course']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="course-detail-view">
        <div class="form-group field-course-season_id required">
            <label class="control-label" for="view-course-season_id">Season ID</label>
            <input type="text" id="view-course-season_id" class="form-control" name="season_id"
                   value="<?= $model->season->title ?>" readonly="readonly" maxlength="255">
        </div>
        <div class="form-group field-course-title required">
            <label class="control-label" for="view-course-title">Title</label>
            <input type="text" id="view-course-title" class="form-control" name="title"
                   value="<?= $model->title ?>" readonly="readonly" maxlength="255">
        </div>
        <div class="form-group field-course-description">
            <label class="control-label" for="view-course-description">Description</label>
            <input type="text" id="view-course-description" class="form-control" name="description"
                   value="<?= $model->description ?>" readonly="readonly" maxlength="255">
        </div>
    </div>

    <?php
    if (!empty($seasonsList)) {
        echo $this->render('_form', [
            'model' => $model,
            'seasonsList' => $seasonsList,
            'isHidden' => true,
            'formAction' => Url::to(['course/update/' . $model->id]),
        ]);
    } else {
        echo Html::tag('p', 'seasonsList is empty!');
    }
    ?>

    <h1>Course Exams:</h1>
    <div class="items exams clearfix">
        <?php
        if (empty($exams)) {
            ?>
        <h1 class="alert">There no exams has been created!</h1>
            <?php
        }
        ?>
        <?= Html::a('Create exam', ['exam/create'], ['class' => 'btn btn-primary create-exam']); ?>

        <div class="items-wrap clearfix">
        <?php
        foreach ($exams as $exam) {
            ?>
            <div class="item exam">
                <h1><?= $exam->title; ?></h1>
                <p class="description"><?= $exam->description; ?></p>
                <?= Html::a('View exam', ['exam/view', 'id' => $exam->id], ['class' => 'btn btn-success']); ?>
            </div>
            <?php
        }
        ?>
        </div>
    </div>
</div>

<?php
$scriptJS = <<< JS

// Course
$('.update-course').on('click', function()
{
    $('.course-form').removeClass('hidden');
    $('.course-detail-view').addClass('hidden');
    $(this).addClass('hidden');
    return false;
});

$('form#form{$model->formName()} button[type="reset"]').on('click', function()
{
    $('.course-form').addClass('hidden');
    $('.course-detail-view').removeClass('hidden');
    $('.update-course').removeClass('hidden');
    return false;
});

$('form#form{$model->formName()}').on('beforeSubmit', function()
{
    var S_form = $(this);

    if (S_form.find('.has-error').size()) {
        alert('form#form{$model->formName()}  has-error!');
        return false;
    }

    var formData = S_form.serialize();

    $.ajax({
        url: S_form.attr("action"),
        type: S_form.attr("method"),
        data: formData,
        success: function (data) {
            console.log(data);
            $('#view-course-season_id').val(data.season_title);
            $('#view-course-title').val(data.title);
            $('#view-course-description').val(data.description);
            $('form#form{$model->formName()} button[type="reset"]').click();
        },
        error: function () {
            alert("Something went wrong! {$model->formName()}");
        }
    });
}).on('submit', function(e){
    e.preventDefault();
});

JS;
$this->registerJs($scriptJS);