<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $form yii\widgets\ActiveForm */
/* @var $seasonsList array */
/* @var $isHidden boolean */
/* @var $formAction string */

if (empty($isHidden) || $isHidden != true) {
    $isHidden = false;
}
?>

<div class="course-form <?= ($isHidden) ? 'hidden' : ''; ?>">

    <?php
    $formParam = ['id' => 'form' . $model->formName()];
    if (!empty($formAction)) {
        $formParam['action'] = $formAction;
    }

    $form = ActiveForm::begin($formParam); ?>

    <?= $form->field($model, 'season_id')->dropDownList($seasonsList) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= ($isHidden) ? Html::resetButton('Cancel', ['class' => 'btn btn-primary']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
