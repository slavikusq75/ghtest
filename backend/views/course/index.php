<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $newCourse common\models\Course */
/* @var $seasonsList array */

$this->title = 'Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="course-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php if (!Yii::$app->user->isGuest && Yii::$app->user->can('indexUser')) { ?>

            <p>
                <?= Html::a('Create Course', ['create'], ['class' => 'btn btn-success course-create']) ?>
            </p>

        <?php } ?>

        <?= $this->render('_form', [
            'model' => $newCourse,
            'seasonsList' => $seasonsList,
            'isHidden' => true,
            'formAction' => Url::to(['course/create']),
        ]) ?>

        <div class="items courses clearfix">
            <?php
            $courses = $dataProvider->getModels();
            if (empty($courses)) {
                ?>
                <h1 class="alert">There no courses has been created!</h1>
                <?php
            }
            ?>
            <div class="items-wrap clearfix">
                <?php
                foreach ($courses as $course) {
                    ?>
                    <div class="item course">
                        <div
                            class="title"><?= Html::a(Html::encode($course->title), ['course/view', 'id' => $course->id]) ?></div>
                        <div class="description">
                            <p><?= Html::encode($course->description) ?></p>
                        </div>
                        <div class="actions">
                            <?= Html::a('View Course', ['view', 'id' => $course->id], ['class' => 'btn btn-success']); ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>

<?php
$scriptJS = <<< JS

// Course
var courseTemplate = function(courseId, courseTitle, courseDescription)
{
    var html = '<div class="item course"> ' +
            '   <div class="title"><a href="/course/view?id=%%courseId%%">%%courseTitle%%</a></div> ' +
            '   <div class="description"><p>%%courseDescription%%</p></div> ' +
            '   <div class="actions"> ' +
            '       <a class="btn btn-success" href="/course/view?id=%%courseId%%">View Course</a> ' +
            '   </div> ' +
            '</div>';
    var result = '';
    html = html.replace(new RegExp('%%courseId%%', 'g'), courseId);
    html = html.replace(new RegExp('%%courseTitle%%', 'g'), courseTitle);
    result = html.replace(new RegExp('%%courseDescription%%', 'g'), courseDescription);
    return result;
};

// clear fields of course create form
var courseFormClear = function()
{
    $('form#form{$newCourse->formName()} input[type="text"]').val('');
    $('.course-index .course-create').removeClass('hidden');
    $('.course-form').addClass('hidden');
};

// show course create form
$('.course-index .course-create').on('click', function()
{
    $('.course-index .course-create').addClass('hidden');
    $('.course-form').removeClass('hidden');
    $('#course-title').focus();
    return false;
});

// hide  course create form
$('form#form{$newCourse->formName()} button[type="reset"]').on('click', function()
{
    $('.course-form').addClass('hidden');
    courseFormClear();
    return false;
});

$('form#form{$newCourse->formName()}').on('beforeSubmit', function()
{
    var S_form = $(this);

    if (S_form.find('.has-error').size()) {
        alert('form#form{$newCourse->formName()}  has-error!');
        return false;
    }

    var formData = S_form.serialize();

    $.ajax({
        url: S_form.attr("action"),
        type: S_form.attr("method"),
        data: formData,
        success: function (data) {
            var course = courseTemplate(data.id, data.title, data.description);
            $('.items.courses .items-wrap').append(course);
            courseFormClear();
        },
        error: function () {
            alert("Something went wrong! {$newCourse->formName()}");
        }
    });
}).on('submit', function(e){
    e.preventDefault();
});

JS;
$this->registerJs($scriptJS);