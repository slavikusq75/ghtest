<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 12.04.16
 * Time: 12:39
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $inviteSearch common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $inviteDataProvider yii\data\ActiveDataProvider */

$this->title = 'Invited teachers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $inviteDataProvider,
        'filterModel' => $inviteSearch,
        'perfectScrollbar' => true,
        'floatHeader' => true,
        'responsive' => true,
        'hover' => true,
        'panel' => [
            'heading' => '<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i>Invited teachers</h3>',
            'type' => 'success',
            'footer' => false
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            'username',
            'firstname',
            'lastname',
            'invite_status' => [
                'attribute' => 'teacher_token',
                'label' => 'Invite\'s status',
                'value' => function ($data) {
                    if (!User::isTeacherTokenExpire($data->teacher_token) && $data->status == 10) {
                        return 'Accepted';
                    } elseif (User::isTeacherTokenExpire($data->teacher_token) && $data->status == 1) {
                        return 'In wait';
                    } elseif (User::isTeacherTokenExpire($data->teacher_token) && $data->status == 10) {
                        return 'Accepted';
                    } elseif (!User::isTeacherTokenExpire($data->teacher_token) && $data->status == 1) {
                        return 'Expired';
                    }
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '',
            ],
        ],
    ]); ?>

</div>
