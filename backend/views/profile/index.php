<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $urlUserPhoto string */

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= empty($urlUserPhoto) ? '' : Html::img($urlUserPhoto, ['alt' => 'My photo']) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'email:email',
            'phone',
            'firstname',
            'middlename',
            'lastname',
        ],
    ]) ?>

</div>
