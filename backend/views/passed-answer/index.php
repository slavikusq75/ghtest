<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PassedAnswerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Passed Answers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-answer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= (!Yii::$app->user->can('createPassedAnswer')) ? '' :
            Html::a('Create Passed Answer', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'passed_question_id',
            'answer_id',
            'right',
            'status',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('viewPassedAnswer'),
                    'update' => Yii::$app->user->can('updatePassedAnswer'),
                    'delete' => Yii::$app->user->can('deletePassedAnswer'),
                ],
            ],
        ],
    ]); ?>

</div>
