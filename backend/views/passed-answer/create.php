<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PassedAnswer */

$this->title = 'Create Passed Answer';
$this->params['breadcrumbs'][] = ['label' => 'Passed Answers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-answer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
