<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Exam */
/* @var $coursesList array */
/* @var $modelsQuestion array */
/* @var $modelsAnswer array */

$this->title = 'Update Exam: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$titleQuestions = 'Questions';

?>
<div class="exam-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formdynamic', [
        'model' => $model,
        'coursesList' => $coursesList,
        'modelsQuestion' => $modelsQuestion,
        'modelsAnswer' => $modelsAnswer,
    ]) ?>

</div>

