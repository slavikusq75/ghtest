<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Exam */
/* @var $form yii\widgets\ActiveForm */
/* @var $coursesList array */
/* @var $modelsQuestion array */
/* @var $modelQuestion common\models\Question */
/* @var $modelsAnswer array */

?>

<div class="exam-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

    <?= $form->field($model, 'course_id')->dropDownList($coursesList) ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'timer')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'date_begin')->widget(DatePicker::className(), [
                'dateFormat' => 'MM/dd/yyyy'
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
                'dateFormat' => 'MM/dd/yyyy'
            ]) ?>
        </div>
    </div>


    <div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div>

    <?php DynamicFormWidget::begin([
        'widgetContainer' => 'dynamicform_wrapper',
        'widgetBody' => '.container-items',
        'widgetItem' => '.question-item',
        'limit' => 10,
        'min' => 1,
        'insertButton' => '.add-question',
        'deleteButton' => '.remove-question',
        'model' => $modelsQuestion[0],
        'formId' => 'dynamic-form',
        'formFields' => [
            'exam_id',
            'title',
            'description',
            'point',
        ],
    ]); ?>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>Question</th>
            <th style="width: 450px;">Answers</th>
            <th class="text-center" style="width: 90px;">
                <button type="button" class="add-question btn btn-success btn-xs"><span class="fa fa-plus"></span></button>
            </th>
        </tr>
        </thead>
        <tbody class="container-items">
        <?php
        foreach ($modelsQuestion as $indexQuestion => $modelQuestion) {
            ?>
            <tr class="question-item">
                <td class="vcenter">
                    <?php
                    // necessary for update action.
                    if (!$modelQuestion->isNewRecord) {
                        echo Html::activeHiddenInput($modelQuestion, "[{$indexQuestion}]id");
                    }
                    ?>
                    <?= $form->field($modelQuestion, "[{$indexQuestion}]title")->textInput(['maxlength' => true]) ?>
                    <?= $form->field($modelQuestion, "[{$indexQuestion}]description")->textInput(['maxlength' => true]) ?>
                    <?= $form->field($modelQuestion, "[{$indexQuestion}]point")->textInput(['maxlength' => true]) ?>
                </td>
                <td>
                    <?php
                                        echo $this->render('_form-answers', [
                                            'form' => $form,
                                            'indexQuestion' => $indexQuestion,
                                            'modelsAnswer' => $modelsAnswer[$indexQuestion],
                                        ]);
                    ?>
                </td>
                <td class="text-center vcenter" style="width: 90px; verti">
                    <button type="button" class="remove-question btn btn-danger btn-xs">
                        <span class="fa fa-minus"></span>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php DynamicFormWidget::end(); ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a( 'Cancel', Yii::$app->request->referrer, ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

