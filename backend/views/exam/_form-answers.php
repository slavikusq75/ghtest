<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;

/* @var $modelsAnswer array */
/* @var $modelAnswer common\models\Answer */
/* @var $indexQuestion integer */

?>

<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_inner',
    'widgetBody' => '.container-answers',
    'widgetItem' => '.answer-item',
    'limit' => 4,
    'min' => 1,
    'insertButton' => '.add-answer',
    'deleteButton' => '.remove-answer',
    'model' => $modelsAnswer[0],
    'formId' => 'dynamic-form',
    'formFields' => [
        'description',
        'right'
    ],
]); ?>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Description</th>
            <th class="text-center">
                <button type="button" class="add-answer btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></button>
            </th>
        </tr>
        </thead>
        <tbody class="container-answers">
        <?php foreach ($modelsAnswer as $indexAnswer => $modelAnswer): ?>
            <tr class="answer-item">
                <td class="vcenter">
                    <?php
                    // necessary for update action.
                    if (! $modelAnswer->isNewRecord) {
                        echo Html::activeHiddenInput($modelAnswer, "[{$indexQuestion}][{$indexAnswer}]id");
                    }
                    ?>
                    <?= $form->field($modelAnswer, "[{$indexQuestion}][{$indexAnswer}]description")->textInput(['maxlength' => true]) ?>
                    <?= $form->field($modelAnswer, "[{$indexQuestion}][{$indexAnswer}]right")->checkbox() ?>
                </td>
                <td class="text-center vcenter" style="width: 90px;">
                    <button type="button" class="remove-answer btn btn-danger btn-xs"><span class="glyphicon glyphicon-minus"></span></button>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php DynamicFormWidget::end(); ?>