<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Exam */
/* @var $question common\models\Question */
/* @var $questionDataProvider yii\data\ActiveDataProvider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$titleQuestions = 'Questions';

?>
<div class="exam-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="exam-detail-view">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group field-exam-season_id">
                    <label class="control-label"
                           for="view-exam-course"><?= $model->course->formName() ?></label>
                    <?= Html::input('text', 'course', $model->course->title, [
                        'id' => 'view-exam-course',
                        'class' => 'form-control',
                        'readonly' => true
                    ]) ?>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group field-exam-title">
                    <label class="control-label" for="view-exam-title"><?= $model->getAttributeLabel('title') ?></label>
                    <?= Html::input('text', 'title', $model->title, [
                        'id' => 'view-exam-title',
                        'class' => 'form-control',
                        'readonly' => true
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group field-exam-description">
                    <label class="control-label"
                           for="view-exam-description"><?= $model->getAttributeLabel('description') ?></label>
                    <?= Html::input('text', 'description', $model->description, [
                        'id' => 'view-exam-description',
                        'class' => 'form-control',
                        'readonly' => true
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group field-exam-timer">
                    <label class="control-label" for="view-exam-timer"><?= $model->getAttributeLabel('timer') ?></label>
                    <?= Html::input('text', 'timer', $model->timer, [
                        'id' => 'view-exam-timer',
                        'class' => 'form-control',
                        'readonly' => true
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <div class="form-group field-exam-date_begin">
                    <label class="control-label"
                           for="exam-date_begin"><?= $model->getAttributeLabel('date_begin') ?></label>
                    <?= Html::input('text', 'date_begin', date('m/d/Y', $model->date_begin), [
                        'id' => 'exam-date_begin',
                        'class' => 'form-control',
                        'readonly' => true
                    ]) ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group field-exam-date_end">
                    <label class="control-label"
                           for="exam-date_end"><?= $model->getAttributeLabel('date_end') ?></label>
                    <?= Html::input('text', 'date_end', date('m/d/Y', $model->date_end), [
                        'id' => 'exam-date_end',
                        'class' => 'form-control',
                        'readonly' => true

                    ]) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="padding-v-md">
        <div class="line line-dashed"></div>
    </div>

    <div class="dynamicform_wrapper" data-dynamicform="dynamicform_29785c0a">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>NN</th>
                <th>Question</th>
                <th style="width: 450px;">Answers</th>
            </tr>
            </thead>
            <tbody class="container-items">
            <?php
            $questions = $questionDataProvider->getModels();
            foreach ($questions as $indexQuestion => $question) {
                ?>
                <tr class="question-item">
                    <td class="vcenter">
                        <label class="control-label"><?= $indexQuestion+1 ?></label>
                    </td>
                    <td class="vcenter">
                        <div class="form-group field-question-title">
                            <label class="control-label">Title</label>
                            <?= Html::input('text', 'title', $question->title, [
                                'class' => 'form-control',
                                'readonly' => true,
                                'maxlength' => '255'
                            ]) ?>
                        </div>
                        <div class="form-group field-question-description">
                            <label class="control-label">Description</label>
                            <?= Html::input('text', 'description', $question->description, [
                                'class' => 'form-control',
                                'readonly' => true,
                                'maxlength' => '255'
                            ]) ?>
                        </div>
                        <div class="form-group field-question-point">
                            <label class="control-label">Point</label>
                            <?= Html::input('text', 'description', $question->point, [
                                'class' => 'form-control',
                                'readonly' => true,
                                'maxlength' => '255'
                            ]) ?>
                        </div>
                    </td>
                    <td>
                        <div class="dynamicform_inner" data-dynamicform="dynamicform_e6c09fc8">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Right</th>
                                </tr>
                                </thead>
                                <tbody class="container-answers">
                                <?php
                                foreach ($question->answers as $answer) {
                                    ?>
                                    <tr class="answer-item">
                                        <td class="vcenter">
                                            <div class="form-group field-answer-0-0-description required has-error">
                                                <p><?= $answer->description ?></p>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group field-answer-0-0-right required">
                                                <p><?= ($answer->right) ? 'Right' : '' ?></p>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
