<?php

use common\models\Exam;
use yii\helpers\Html;
use kartik\grid\GridView;
use \yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ExamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $coursesList array */

$this->title = 'Exams';
$this->params['breadcrumbs'][] = $this->title;

$titleQuestions = 'Questions';
$titleAnswers = 'Answers';

?>
<div class="exam-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(['id' => 'examsGrid']); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'perfectScrollbar' => true,
        'floatHeader' => true,
        'responsive'=>true,
        'hover'=>true,
        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-globe"></i> Exams</h3>',
            'type'=>'success',
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Exam', ['create'], ['class' => 'btn btn-success']),
            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
            'footer'=>false
        ],
        'pjax'=>true,
        'pjaxSettings'=>['neverTimeout'=>true],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'course_id' => [
                'attribute' => 'course_id',
                'label' => 'Course',
                'filter' => $coursesList,
                'value' => function(Exam $model) {
                    return $model->course->title;
                },
            ],
            'title',
            'description',
            'timer',

            ['class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'view' => Yii::$app->user->can('viewExam'),
                    'update' => Yii::$app->user->can('updateExam'),
                    'delete' => Yii::$app->user->can('deleteExam'),
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

