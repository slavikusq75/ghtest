<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Exam */
/* @var $coursesList array */
/* @var $modelsQuestion array */
/* @var $modelsAnswer array */

$this->title = 'Create Exam';
$this->params['breadcrumbs'][] = ['label' => 'Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exam-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formdynamic', [
        'model' => $model,
        'coursesList' => $coursesList,
        'modelsQuestion' => $modelsQuestion,
        'modelsAnswer' => $modelsAnswer,
    ]) ?>

</div>
