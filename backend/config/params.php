<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'slavikusq75@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'user.teacherTokenExpire' => 3600,
];
