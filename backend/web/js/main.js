/**
 * Created by slava on 28.02.16.
 */
$(function(){
    $('#modalButtonExam').click(function(){
       $('#modalExam').modal('show')
           .find('#modalContentExam')
           .load($(this).attr('value'));
    });
});

$(function(){
    $('#modalButtonQuest').click(function(){
        $('#modalQuest').modal('show')
            .find('#modalContentQuest')
            .load($(this).attr('value'));
    });
});

$(function(){
    $('#modalButtonAnsw').click(function(){
        $('#modalAnsw').modal('show')
            .find('#modalContentAnsw')
            .load($(this).attr('value'));
    });
});