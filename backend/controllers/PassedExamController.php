<?php

namespace backend\controllers;

use common\models\QuestionSearch;
use Yii;
use common\models\PassedExam;
use common\models\PassedExamSearch;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\models\Course;
use common\models\Exam;
use common\models\Question;
use common\models\Answer;

/**
 * PassedExamController implements the CRUD actions for PassedExam model.
 */
class PassedExamController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PassedExam models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PassedExamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $userID = Yii::$app->user->identity->getId();
        $passedExams = PassedExam::find()
            ->where(['user_id' => $userID])
//                 if timer is null then exam has not been passed
            ->andWhere(['is not', 'timer', null])
            ->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'passedExams' => $passedExams,
            'resultDataProvider' => $dataProvider,
            'coursesList'  => $this->getCoursesList(),
            'examsList' => $this->getExamsList(),
        ]);
    }

    /**
     * Displays a single PassedExam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewPassedExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);

        $questionSearch = new QuestionSearch();
        $questionDataProvider = $questionSearch->passedQuestionSearch($model->exam_id, $model->id, Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'questionDataProvider' => $questionDataProvider,
            'questionSearch' => $questionSearch,
        ]);
    }

    /**
     * Creates a new PassedExam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PassedExam();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PassedExam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PassedExam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PassedExam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PassedExam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PassedExam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getCoursesList()
    {
        $courses = Course::find()->where(['status' => 1])->asArray()->all();
        $coursesList = ArrayHelper::map($courses, 'id', 'title');
        return $coursesList;
    }

    protected function getExamsList()
    {
        $exams = Exam::find()->where(['status' => 1])->asArray()->all();
        $examsList = ArrayHelper::map($exams, 'id', 'title');
        //var_dump($examsList); exit();
        return $examsList;
    }

}
