<?php

namespace backend\controllers;

use Yii;
use yii\db\Exception;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use common\models\Exam;
use common\models\ExamSearch;
use common\models\MultipleModel;
use common\models\Course;
use common\models\Question;
use common\models\Answer;


/**
 * ExamController implements the CRUD actions for Exam model.
 */
class ExamController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Exam models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ExamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=5;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'coursesList'  => $this->getCoursesList(),
        ]);
    }

    /**
     * Displays a single Exam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $questionDataProvider = new ActiveDataProvider([
            'query' => Question::find()->where(['status' => 1])->with('exam')->where(['exam_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'questionDataProvider' => $questionDataProvider,
        ]);
    }

    /**
     * Creates a new Exam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateorigin()
    {
        if (!Yii::$app->user->can('createExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = new Exam();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'coursesList' => $this->getCoursesList(),
                'modelsQuestion' => [new Question()],
                'modelsAnswer' => [[new Answer()]],
            ]);
        }
    }


    /**
     * Creates a new Exam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        if (!Yii::$app->user->can('createExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = new Exam();
        $modelsQuestion = [];
        $modelsAnswer = [[new Answer()]];

        if ($this->examLoadValidate($model, $modelsQuestion, $modelsAnswer)) {

            $model->date_begin = strtotime($model->date_begin);
            $model->date_end = strtotime($model->date_end);

            if ($this->examSave($model, $modelsQuestion, $modelsAnswer)) {

                return $this->redirect(['view', 'id' => $model->id]);

            }
        } else {
            $this->ifErrorSetFlash($model, $modelsQuestion, $modelsAnswer);

            if (empty($modelsQuestion)) {
                $modelsQuestion = [new Question()];
            }
        }

        return $this->render('create', [
            'model' => $model,
            'coursesList' => $this->getCoursesList(),
            'modelsQuestion' => (empty($modelsQuestion)) ? [new Question()] : $modelsQuestion,
            'modelsAnswer' => (empty($modelsAnswer)) ? [[new Answer()]] : $modelsAnswer,
        ]);

    }

    /**
     * Updates an existing Exam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateorigin($id)
    {
        if (!Yii::$app->user->can('updateExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = $model->toArray(['id', 'course_id', 'title', 'description', 'timer']);
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = ['message', 'Error occurred!'];
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'coursesList' => $this->getCoursesList(),
                ]);
            }
        }
    }

    /**
     * Updates an existing Exam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);
        $modelsQuestion = $model->questions;
        $modelsAnswer = [];
        $oldAnswers = [];
        $oldQuestionIDs = [];
        $answersIDs = [];

        if (!empty($modelsQuestion)) {
            $oldQuestionIDs = ArrayHelper::map($modelsQuestion, 'id', 'id');

            foreach ($modelsQuestion as $indexQuestion => $modelQuestion) {
                $answers = $modelQuestion->answers;
                $modelsAnswer[$indexQuestion] = $answers;
                $oldAnswers = ArrayHelper::merge(ArrayHelper::index($answers, 'id'), $oldAnswers);
            }
        }

        if ($this->examLoadValidate($model, $modelsQuestion, $modelsAnswer, $oldAnswers)) {
            if (isset($_POST['Answer'][0][0])) {
                foreach ($_POST['Answer'] as $answers) {
                    $answersIDs = ArrayHelper::merge($answersIDs, array_filter(ArrayHelper::getColumn($answers, 'id')));
                }
            }
            $oldAnswersIDs = ArrayHelper::getColumn($oldAnswers, 'id');
            $deletedAnswersIDs = array_diff($oldAnswersIDs, $answersIDs);
            $deletedQuestionIDs = array_diff($oldQuestionIDs, array_filter(ArrayHelper::map($modelsQuestion, 'id', 'id')));

            $model->date_begin = strtotime($model->date_begin);
            $model->date_end = strtotime($model->date_end);

            if ($this->examSave($model, $modelsQuestion, $modelsAnswer, $deletedQuestionIDs, $deletedAnswersIDs)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $this->ifErrorSetFlash($model, $modelsQuestion, $modelsAnswer);
        }

        return $this->render('update', [
            'model' => $model,
            'coursesList' => $this->getCoursesList(),
            'modelsQuestion' => (empty($modelsQuestion)) ? [new Question] : $modelsQuestion,
            'modelsAnswer' => (empty($modelsAnswer)) ? [[new Answer]] : $modelsAnswer
        ]);
    }

    /**
     * Deletes an existing Exam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('deleteExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);
        $model->status = 0;
        $isSave = $model->save(false);
        if ($isSave) {
            foreach ($model->questions as $question) {
                $question->status = 0;
                if ($question->save()) {
                    foreach ($question->answers as $answer) {
                        $answer->status = 0;
                        $answer->save();
                    }
                }
            }
        }
        $message = ($isSave) ? 'Exam has been deleted successfully!' : 'Exam has not been deleted. Error occurred!';

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = ['message' => $message, 'isDeleted' => $isSave];
        } else {
            Yii::$app->session->flash = $message;
            return $this->redirect(['index']);
        }
    }

    /**
     * Hard-Deletes an existing Exam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletehard($id)
    {
        if (!Yii::$app->user->can('deleteExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Exam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Exam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exam::findOne(['id' => $id, 'status' => 1])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getCoursesList()
    {
        $courses = Course::find()->where(['status' => 1])->asArray()->all();
        $coursesList = ArrayHelper::map($courses, 'id', 'title');
        return $coursesList;
    }

    /**
     * Load and validate exam with questions and answers
     *
     * @param Exam $model
     * @param array $modelsQuestion array(Question)
     * @param array $modelsAnswer array(array(Answer))
     * @param array $oldAnswers array(Answer)
     * @return bool result of exam validation
     */
    private function examLoadValidate(Exam $model, array &$modelsQuestion, array &$modelsAnswer, array $oldAnswers = [])
    {
        /* @var $modelQuestion Question */
        /* @var $modelAnswer Answer */

        $valid = false;
        if ($model->load(Yii::$app->request->post())) {

            $modelsQuestion = MultipleModel::createMultiple(Question::classname(), $modelsQuestion);
            MultipleModel::loadMultiple($modelsQuestion, Yii::$app->request->post());
            // set Question->exam_id = 0 to make it valid
            foreach ($modelsQuestion as $modelQuestion) {
                if (empty($modelQuestion->exam_id)) {
                    $modelQuestion->exam_id = 0;
                }
            }

            // validate exam and questions models
            $valid = $model->validate();
            $valid = MultipleModel::validateMultiple($modelsQuestion) && $valid;

            if (isset($_POST['Answer'][0][0])) {
                foreach ($_POST['Answer'] as $indexQuestion => $answers) {
                    foreach ($answers as $indexAnswer => $answer) {
                        $data['Answer'] = $answer;
                        $modelAnswer = (isset($answer['id']) && !empty($oldAnswers) && isset($oldAnswers[$answer['id']])) ? $oldAnswers[$answer['id']] : new Answer;
                        $modelAnswer->load($data);
                        // set Answer->question_id = 0 to make it valid
                        if (empty($modelAnswer->question_id)) {
                            $modelAnswer->question_id = 0;
                        }
                        $modelsAnswer[$indexQuestion][$indexAnswer] = $modelAnswer;
                        $valid = $modelAnswer->validate() && $valid;
                    }
                }
            }
        }

        return $valid;
    }

    /**
     * Save exam with questions and answers
     *
     * @param Exam $model
     * @param array $modelsQuestion array(Question)
     * @param array $modelsAnswer array(array(Answer))
     * @param array $deletedQuestionIDs
     * @param array $deletedAnswersIDs
     * @return bool result of exam validation
     */
    private function examSave(Exam $model, array $modelsQuestion, array $modelsAnswer, array $deletedQuestionIDs = [], array $deletedAnswersIDs = [])
    {
        /* @var $modelQuestion Question */
        /* @var $modelAnswer Answer */

        $flag = false;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($flag = $model->save(false)) {

                if (!empty($deletedAnswersIDs)) {
                    Answer::deleteAll(['id' => $deletedAnswersIDs]);
                }

                if (!empty($deletedQuestionIDs)) {
                    Question::deleteAll(['id' => $deletedQuestionIDs]);
                }

                foreach ($modelsQuestion as $indexQuestion => $modelQuestion) {

                    if ($flag === false) {
                        break;
                    }

                    $modelQuestion->exam_id = $model->id;

                    if (!($flag = $modelQuestion->save(false))) {
                        break;
                    }

                    if (isset($modelsAnswer[$indexQuestion]) && is_array($modelsAnswer[$indexQuestion])) {
                        foreach ($modelsAnswer[$indexQuestion] as $indexAnswer => $modelAnswer) {
                            $modelAnswer->question_id = $modelQuestion->id;
                            if (!($flag = $modelAnswer->save(false))) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($flag) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
            }
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return $flag;
    }

    /**
     * Setting Flash messages if models have errors
     *
     * @param Exam $model
     * @param array $modelsQuestion array of Question
     * @param array $modelsAnswer array of Answer
     */
    private function ifErrorSetFlash(Exam $model, array $modelsQuestion, array $modelsAnswer)
    {
        /* @var $modelQuestion \common\models\Question */
        /* @var $modelAnswer \common\models\Answer */

        $errors = [];
        $errors = array_merge($errors, $model->errors);

        foreach ($modelsQuestion as $key => $modelQuestion) {
            if ($modelQuestion->hasErrors()) {
                $errors = array_merge($errors, $modelQuestion->errors);
            }
            if (!empty($modelsAnswer[$key])) {
                foreach ($modelsAnswer[$key] as $modelAnswer) {
                    if ($modelAnswer->hasErrors()) {
                        $errors = array_merge($errors, $modelAnswer->errors);
                    }
                }
            }
        }

        if (!empty($errors)) {
            Yii::$app->session->setFlash('error', Html::listBox('examErrors', null, $errors));
        }
    }

}
