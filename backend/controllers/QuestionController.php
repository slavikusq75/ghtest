<?php

namespace backend\controllers;

use Yii;
use common\models\Exam;
use common\models\Question;
use common\models\QuestionSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\Answer;
use yii\web\Response;


/**
 * QuestionController implements the CRUD actions for Question model.
 */
class QuestionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Question models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'examsList' => $this->getExamsList(),
        ]);
    }

    /**
     * Displays a single Question model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (!Yii::$app->user->can('viewQuestion')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $answerDataProvider = new ActiveDataProvider([
            'query' => Answer::find()->with('question')->where(['question_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'answerDataProvider' => $answerDataProvider,
        ]);
    }

    /**
     * Creates a new Question model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createQuestion')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = new Question();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = $model->toArray(['id', 'exam_id', 'title', 'description', 'type', 'point']);
                Yii::$app->response->data['exam_title'] = $model->exam->title;
                Yii::$app->response->data['message'] = 'The Question has been created successfully!';
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = ['message' => 'The Question has not been created. Error occured!'];
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'examsList' => $this->getExamsList(),
                ]);
            }
        }
    }

    /**
     * Updates an existing Question model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->can('updateQuestion')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'examsList' => $this->getExamsList(),
            ]);
        }
    }

    /**
     * Deletes an existing Question model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->user->can('deleteQuestion')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);
        $answersCount = count($model->answers);
        if ($answersCount > 0) {
            $isSave = false;
            $message = 'The question has answers. You must delete them first.';
        } else {
            $model->status = 0;
            $isSave = $model->save();
            $message = ($isSave) ? 'The Question has been deleted successfully!' : 'The Question has not been deleted. Error occurred!';
        }


        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = ['message' => $message, 'isDeleted' => $isSave];
        } else {
            Yii::$app->session->flash = $message;
            return $this->redirect(['index']);
        }
    }

    /**
     * Hard-Deletes an existing Question model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletehard($id)
    {
        if (!Yii::$app->user->can('deleteAnswer')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->redirect(['index']);
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Question model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Question the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Question::findOne(['id' => $id, 'status' => 1])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function getExamsList()
    {
        $exams = Exam::find()->where(['status' => 1])->asArray()->all();
        $examsList = ArrayHelper::map($exams, 'id', 'title');
        return $examsList;
    }
}
