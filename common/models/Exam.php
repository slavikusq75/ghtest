<?php

namespace common\models;

use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\components\validators\ExamBeginEndDateValidator;

/**
 * This is the model class for table "exam".
 *
 * @property integer $id
 * @property integer $course_id
 * @property string $title
 * @property string $description
 * @property integer $timer
 * @property integer $date_begin
 * @property integer $date_end
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Course $course
 * @property PassedExam[] $passedExams
 * @property Question[] $questions
 */
class Exam extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exam}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'title', 'timer', 'date_begin', 'date_end'], 'required'],
            [['course_id', 'timer', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
            [['date_begin', 'date_end'], 'date', 'format' => 'MM/dd/yyyy'],
            [['date_begin', 'date_end'], ExamBeginEndDateValidator::className()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'title' => 'Title',
            'description' => 'Description',
            'timer' => 'Timer',
            'date_begin' => 'Begin date',
            'end_begin' => 'End date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedExams()
    {
        return $this->hasMany(PassedExam::className(), ['exam_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['exam_id' => 'id']);
    }

    public function calcExamPoint($examId, $userId)
    {
        $pointExam = 0;
        $resPassedQuestions = [];

        $arrPassedExam = PassedExam::find()->where(['exam_id' => $examId, 'user_id' => $userId])
            ->select(['id', 'user_id', 'exam_id', 'timer'])
            ->with(['passedQuestions' => function($pq) {
                $pq->select(['id', 'passed_exam_id', 'question_id', 'point'])
                    ->with(['question' => function($q) {
                        $q->select(['id', 'title', 'point'])
                            ->with(['answers' => function($a) {
                                $a->select(['id', 'question_id', 'description', 'right']);
                            }]);
                    }])
                    ->with(['passedAnswers' => function($pa) {
                        $pa->select(['id', 'passed_question_id', 'answer_id', 'right']);
                    }]);
            }])->asArray()->all();

        if (!empty($arrPassedExam)) {
            foreach ($arrPassedExam[0]['passedQuestions'] as $arrPassedQuestion) {
                $questionId = $arrPassedQuestion['question_id'];
                foreach ($arrPassedQuestion['passedAnswers'] as $key => $arrPassedAnswer) {
                    $resPassedAnswers[$arrPassedAnswer['answer_id']] = $arrPassedAnswer['right'];
                }
                foreach ($arrPassedQuestion['question']['answers'] as $key => $answer) {
                    if ($answer['right']) {
                        $answerId = $answer['id'];
                        $passedRight = empty($resPassedAnswers[$answerId]) ? 0 : $resPassedAnswers[$answerId];
                        $resPassedQuestions[$questionId][$answerId] = (int)($answer['right'] == $passedRight);
                    }
                }
                $countAnswers = count($resPassedQuestions[$questionId]);
                $countAnswers = ($countAnswers == 0) ? 1 : $countAnswers; // prevent division by zero
                $sumAnswers = array_sum($resPassedQuestions[$questionId]);
                $pointQuestion = $arrPassedQuestion['question']['point'];
                $point = round(($sumAnswers/$countAnswers)*$pointQuestion, 2);
                $resPassedQuestions[$questionId]['point'] = $point;
                $pointExam += $point;

                $passedQuestion = PassedQuestion::findOne($arrPassedQuestion['id']);
                $passedQuestion->point = $point;
                $passedQuestion->save();
            }
        }

        return ['pointExam' => $pointExam, 'passedQuestions' => $resPassedQuestions];
    }

    public function canBePassed()
    {
        $today = (new DateTime())->getTimestamp();

        return ($today >= $this->date_begin && $today <= $this->date_end);
    }
}
