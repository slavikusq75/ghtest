<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "passed_question".
 *
 * @property integer $id
 * @property integer $passed_exam_id
 * @property integer $question_id
 * @property double $point
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PassedAnswer[] $passedAnswers
 * @property Question $question
 * @property PassedExam $passedExam
 */
class PassedQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%passed_question}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passed_exam_id', 'question_id'], 'required'],
            [['passed_exam_id', 'question_id', 'point', 'status', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passed_exam_id' => 'Passed Exam ID',
            'question_id' => 'Question ID',
            'point' => 'Point',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedAnswers()
    {
        return $this->hasMany(PassedAnswer::className(), ['passed_question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedExam()
    {
        return $this->hasOne(PassedExam::className(), ['id' => 'passed_exam_id']);
    }
}
