<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PassedExam;

/**
 * PassedExamSearch represents the model behind the search form about `common\models\PassedExam`.
 */
class PassedExamSearch extends PassedExam
{

    public $lastname;
    public $title;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'exam_id', 'timer', 'status', 'created_at', 'updated_at'], 'integer'],
            [['lastname'], 'string'],
            [['title'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PassedExam::find()
            ->JoinWith(['exam', 'user', 'passedQuestions']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'exam.id' => $this->id,
            'timer' => $this->timer,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['like', 'user.lastname', $this->lastname])
            ->andFilterWhere(['like', 'exam.title', $this->title]);
        return $dataProvider;
    }
}
