<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "passed_answer".
 *
 * @property integer $id
 * @property integer $passed_question_id
 * @property integer $answer_id
 * @property integer $right
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Answer $answer
 * @property PassedQuestion $passedQuestion
 */
class PassedAnswer extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%passed_answer}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['passed_question_id', 'answer_id'], 'required'],
            [['passed_question_id', 'answer_id', 'right', 'status', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passed_question_id' => 'Passed Question ID',
            'answer_id' => 'Answer ID',
            'right' => 'Right',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedQuestion()
    {
        return $this->hasOne(PassedQuestion::className(), ['id' => 'passed_question_id']);
    }
}
