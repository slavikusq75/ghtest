<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course".
 *
 * @property integer $id
 * @property integer $season_id
 * @property string $title
 * @property string $description
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Season $season
 * @property Exam[] $exams
 * @property UserCourse[] $userCourses
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'title'], 'required'],
            [['season_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255],
            [['title'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'season_id' => 'Season ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Returns active Seasons as array[id, title]
     * @return array
     */
    public function getSeasons()
    {
        $seasons = [];
        foreach (Season::find()->where(['status' => 1])->each() as $season) {
            $seasons[$season->id] = $season->title;
        }
        return $seasons;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(Season::className(), ['id' => 'season_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExams()
    {
        return $this->hasMany(Exam::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(UserCourse::tableName(), ['course_id' => 'id']);
    }

    public function getUsersIdArray()
    {
        $users = $this->getUsers()->where(['>', 'status', 0])->asArray()->all();
        return ArrayHelper::map($users, 'id', 'id');
    }

    /**
     * Check if user with $userId is signed up on course
     * @param $userId integer
     * @return bool
     */
    public function isUserSigned($userId)
    {
        return ArrayHelper::keyExists($userId, $this->getUsersIdArray());
    }
}
