<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "question".
 *
 * @property integer $id
 * @property integer $exam_id
 * @property string $title
 * @property string $description
 * @property integer $type
 * @property integer $point
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Answer[] $answers
 * @property PassedQuestion[] $passedQuestions
 * @property Exam $exam
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%question}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exam_id', 'title'], 'required'],
            [['exam_id', 'type', 'point', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exam_id' => 'Exam ID',
            'title' => 'Title',
            'description' => 'Description',
            'type' => 'Type',
            'point' => 'Point',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['question_id' => 'id'])
            ->where(['status' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedQuestions()
    {
        return $this->hasMany(PassedQuestion::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExam()
    {
        return $this->hasOne(Exam::className(), ['id' => 'exam_id']);
    }
}
