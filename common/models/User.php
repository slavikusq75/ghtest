<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use common\models\InviteTeacherForm;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $firstname
 * @property string $middlename
 * @property string $lastname
 * @property string $photo
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $teacher_token
 */

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 10;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_NOT_ACTIVE]],
            [['username', 'email', 'phone', 'firstname', 'middlename', 'lastname'], 'string', 'max' => 255],
            ['email', 'email'],
            ['phone', 'match', 'pattern' => '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/'],
            ['imageFile', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024],
            ['imageFile', 'image', 'extensions' => 'png, jpg, gif', 'minWidth' => 100, 'maxWidth' => 1000, 'minHeight' => 100, 'maxHeight' => 1000],

            ['teacher_token', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Finds out if teacher token is valid
     *
     * @param string $token teacher token
     * @return boolean
     */
    public static function isTeacherTokenExpire($key)
    {
        if (empty($key)) {
            return false;
        }

        $expire = Yii::$app->params['user.teacherTokenExpire'];
        $parts = explode('_', $key);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    public static function findByTeacherToken($key)
    {
        if(!static::isTeacherTokenExpire($key))

            return null;

        return static::findOne(
            [
                'teacher_token' => $key,
            ]
        );
    }

    public function generateTeacherToken()
    {
        return $this->teacher_token = Yii::$app->security->generateRandomString().'_'.time();
    }

    public function removeTeacherToken()
    {
        $this->teacher_token = null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getPassedExam()
    {
        return $this->hasMany(PassedExam::className(), ['user_id' => 'id']);
    }


    public function save($runValidation = true, $attributeNames = NULL)
    {
        if (!$this->validate()) {
            return false;
        }

        $runValidation = false; // no need validation as model is valid
        $oldPhoto = $this->photo;
        $deleteOldPhoto = false;

        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');

        if (!empty($this->imageFile)) {
            $fileName = $this->imageFile->baseName . '_' . time() . '.' . $this->imageFile->extension;

            if (!\Yii::$app->resourceManager->save($this->imageFile, $fileName)) {
                $deleteOldPhoto = true;
                $this->photo = $fileName;
            }
        }

        if (!parent::save($runValidation, $attributeNames)) {
            return false;
        }

        if (!empty($oldPhoto) && $deleteOldPhoto) {
            if (\Yii::$app->resourceManager->fileExists($oldPhoto)) {
                \Yii::$app->resourceManager->delete($oldPhoto);
            }
        }

        return true;
    }
}
