<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Question;

/**
 * QuestionSearch represents the model behind the search form about `common\models\Question`.
 */
class QuestionSearch extends Question
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'exam_id', 'type', 'point', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Question::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('exam');

        $query->andFilterWhere([
            'id' => $this->id,
            'exam.id' => $this->exam_id,
            'exam.status' => 1,
            'type' => $this->type,
            'point' => $this->point,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param integer $examId
     * @param integer $passedExamId
     * @param array $params
     * @return ActiveDataProvider
     */
    public function passedQuestionSearch($examId, $passedExamId, $params)
    {
        $query = Question::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->where(['question.exam_id' => $examId])
            ->joinWith(['exam' => function($exm) use($passedExamId) {
                $exm->where(['exam.status' => 1])
                    ->joinWith(['passedExams' => function($pe) use($passedExamId) {
                        $pe->where(['passed_exam.id' => $passedExamId])
                            ->joinWith('passedQuestions');
                    }]);
            }]);

        $query->andFilterWhere([
            'id' => $this->id,
            'exam.id' => $this->exam_id,
            'point' => $this->point,
        ]);

        $query->andFilterWhere(['like', 'question.title', $this->title])
            ->andFilterWhere(['like', 'question.description', $this->description]);

        return $dataProvider;
    }
}
