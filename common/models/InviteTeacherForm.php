<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 06.04.16
 * Time: 22:40
 */

namespace common\models;

use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class InviteTeacherForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * Sends an email with a link, for registering new teacher.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = new User();

        if ($user) {
            $teacher_token = $user->generateTeacherToken();
            $user->teacher_token = $teacher_token;
            $user->email = $this->email;
            $user->status = 1;
            $user->save();
        }

        return \Yii::$app->mailer->compose(['html' => 'teacherToken-html'], ['user' => $user])
            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Teacher invite for ' . \Yii::$app->name)
            ->send();
    }
}
