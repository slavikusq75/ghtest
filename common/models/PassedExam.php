<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "passed_exam".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $exam_id
 * @property integer $timer
 * @property integer $started_at
 * @property integer $finished_at
 * @property double $point
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Exam $exam
 * @property User $user
 * @property PassedQuestion[] $passedQuestions
 */
class PassedExam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%passed_exam}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'exam_id'], 'required'],
            [['user_id', 'exam_id', 'timer', 'status', 'started_at', 'finished_at', 'created_at', 'updated_at'], 'integer'],
            [['point'], 'double']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'exam_id' => 'Exam ID',
            'timer' => 'Timer',
            'started_at' => 'Started At',
            'finished_at' => 'Finished At',
            'point' => 'Point',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExam()
    {
        return $this->hasOne(Exam::className(), ['id' => 'exam_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassedQuestions()
    {
        return $this->hasMany(PassedQuestion::className(), ['passed_exam_id' => 'id']);
    }
}
