<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$regTeachLink = Yii::$app->FrontendUrlManager->createAbsoluteUrl(['site/signup', 'token' => $user->teacher_token]);
?>
<div class="password-reset">

    <p>Hello, dear friend <?= Html::encode($user->username) ?>,</p>

    <p>Follow the link below to sign up as a teacher:</p>

    <p><?= Html::a(Html::encode($regTeachLink), $regTeachLink) ?></p>
</div>
