<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['frontend.dev/site/signup', 'token' => $user->teacher_token]);
?>
Hello <?= $user->username ?>,

Follow the link below to sign up as a teacher:

<?= $resetLink ?>
