<?php

namespace common\components\validators;

use common\models\Course;
use DateTime;
use yii\validators\Validator;

class ExamBeginEndDateValidator extends Validator
{
    public $messageCourseNotChosen;

    public $messageCourseWrongChosen;

    public function init()
    {
        parent::init();
        $this->messageCourseNotChosen = 'Course has not been chosen!';
        $this->messageCourseWrongChosen = 'Wrong course has been chosen!';
        $this->message = 'The date need to be in season dates range!';
    }

    public function validateAttribute($model, $attribute)
    {
        if (!$model->hasErrors()) {
            if (empty($model->course_id)) {
                $model->addError($attribute, $this->messageCourseNotChosen);
            } else {
                $course = Course::findOne(['id' => $model->course_id, 'status' => 1]);
                if (empty($course) || empty($course->id)) {
                    $model->addError($attribute, $this->messageCourseWrongChosen);
                } else {
                    $season = $course->season;
                    $dateBegin = new DateTime('01/01/' . $season->year_begin);
                    $dateEnd = new DateTime('12/30/' . $season->year_end);
                    $strDate = $model->$attribute;
                    $date = new DateTime($strDate);
                    if ($date <= $dateBegin || $date >= $dateEnd) {
                        $model->addError($attribute, $this->message);
                    }
                }
            }
        }
    }


    public function clientValidateAttribute($model, $attribute, $view)
    {
        $arrayCourses =[];
        $courses = Course::find()->where(['status' => 1])->all();
        foreach ($courses as $course) {
            $season = $course->season;
            $arrayCourses[$course->id] = [
                'year_begin' => $season->year_begin,
                'year_end' => $season->year_end];
        }

        $arrayCourses = json_encode($arrayCourses);
        $messageCourseNotChosen = json_encode($this->messageCourseNotChosen, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $messageCourseWrongChosen = json_encode($this->messageCourseWrongChosen, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS

        var arrayCourses = $arrayCourses;
        var courseId = $('#exam-course_id').val();
        if (courseId == '') {
            messages.push($messageCourseNotChosen);
        } else {
            var season = arrayCourses[courseId];
            if (season == '') {
                messages.push($messageCourseWrongChosen);
            } else {
                var dateFrom = '01/01/' + season['year_begin'];
                var dateTo = '12/30/' + season['year_end'];

                var from = Date.parse(dateFrom);
                var to   = Date.parse(dateTo);
                var check = Date.parse(value);

                if((check <= from || check >= to)) {
                    messages.push($message);
                }
            }
        }
JS;
    }
}