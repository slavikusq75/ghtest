<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 15.04.16
 * Time: 2:54
 */

namespace common\components\validators;


use common\models\User;
use yii\validators\Validator;

class EmailUniqueValidator extends Validator
{

    public $message;
    public function init()
    {
        parent::init();

        $this->message = 'This email address has been already taken.';
    }

    public function validateAttribute($model, $attribute)
    {
        if (!$model->hasErrors()) {
            $user = User::find()->where(['email' => $model->$attribute])->one();
            if (!empty($user)) {
                if (empty($model->id) || ($model->id != $user->id)) {
                    $model->addError($attribute, $this->message);
                }
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $modelId = '';
        $userId = '';
        $emailTaken = 0;
        if (!$model->hasErrors()) {
            $modelId = empty($model->id) ? '' : $model->id;

            $user = User::find()->where(['email' => $model->$attribute])->one();
            if (!empty($user)) {
                $userId = '';
                $emailTaken = 1;
            }
        }

        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        return <<<JS

        var modelId = $modelId;
        var userId = $userId;
        var emailTaken = $emailTaken;
        var msg = $message;

        if (emailTaken) {
            if ((modelId != '' || modelId != undefined) || (modelId == userId)) {
                messages.push(msg);
            }
        }

JS;
    }
}