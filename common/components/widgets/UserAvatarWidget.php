<?php

namespace common\components\widgets;

use Yii;
use yii\base\Widget;

class UserAvatarWidget extends Widget
{
    public function run()
    {
        $avatar = empty(Yii::$app->user->identity->photo) ? '' : Yii::$app->user->identity->photo;
        if (!empty($avatar) && Yii::$app->resourceManager->fileExists($avatar)) {
            $avatar = Yii::$app->resourceManager->getUrl($avatar);
        } else {
            $avatar = "/img/logo-o.png";
        }

        return $avatar;
    }
}