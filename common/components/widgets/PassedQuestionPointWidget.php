<?php

namespace common\components\widgets;

use yii\base\Widget;

class PassedQuestionPointWidget extends Widget
{
    public $question;

    /**
     * Getting passed question`s point
     * The question property that giving to this widget must contain array of passedQuestions thru relations
     *
     * @return string|integer
     */
    public function run()
    {
        /* @var $passedQuestion \common\models\PassedQuestion */
        /* @var $answer \common\models\Answer */

        if (empty($this->question)) {
            return 'Error: Wrong Question!!!';
        }

        $point = 0;
        $passedQuestions = $this->question->exam->passedExams[0]->passedQuestions;
        foreach ($passedQuestions as $passedQuestion) {
            if ($passedQuestion->question_id === $this->question->id) {
                $point = $passedQuestion->point;
                break;
            }
        }
        return $point;
    }
}