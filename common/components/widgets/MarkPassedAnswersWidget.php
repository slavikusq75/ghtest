<?php

namespace common\components\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;

class MarkPassedAnswersWidget extends Widget
{

    public $question;

    public function init()
    {
        parent::init();
    }

    /**
     * Marking answers for question depend user's answer
     * The question property that giving to this widget must contain array of passedQuestions thru relations
     *
     * @return string
     */
    public function run()
    {
        /* @var $passedQuestion \common\models\PassedQuestion */
        /* @var $answer \common\models\Answer */

        if (empty($this->question)) {
            return 'Error: Wrong Question!!!';
        }

        $str = '<ul class="items answers">';

        $passedQuestions = $this->question->exam->passedExams[0]->passedQuestions;
        $passedAnswers = [];
        foreach ($passedQuestions as $passedQuestion) {
            $pssdAnswers = $passedQuestion->getPassedAnswers()->select(['answer_id', 'right'])->asArray()->all();
            $passedAnswers = array_merge($passedAnswers, $pssdAnswers);
        }
        $rightAnswers = ArrayHelper::map($passedAnswers, 'answer_id', 'right');

        foreach ($this->question->answers as $answer) {
            $checked = ArrayHelper::getValue($rightAnswers, $answer->id);
            $class = ($answer->right) ? ' answer-right' : ' answer-wrong';
            $class .= ($checked) ? ' passed-right' : '';
            $str .= '<li class="item answer ' . $class . '"><span>' . $answer->description . '</span></li>';
        }

        $str .= '</ul>';
        return $str;
    }
}
