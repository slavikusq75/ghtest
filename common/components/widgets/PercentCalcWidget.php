<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 05.04.16
 * Time: 0:54
 */

namespace common\components\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use common\models\Question;


class PercentCalcWidget extends Widget
{

    //public $passedAnswer;
    public $percentcalc;

    public function init(){
        parent::init();
    }

    public function run(){
        $points = 0;
        $questions = $this->percentcalc->exam->questions;
        foreach($questions as $question) {
            $points += $question->point;
        }

        // prevent division by zero
        $points = ($points == 0) ? 1 : $points;

        $formatter = \Yii::$app->formatter;
        $passedPoints = $this->percentcalc->point;
        $percentage = $formatter->asPercent($passedPoints/$points, 2);

        return $percentage;
    }
}
?>