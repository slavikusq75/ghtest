<?php

namespace common\components\widgets;


use yii\base\Widget;
use yii\helpers\ArrayHelper;

class MarkPassedQuestionsAnswersWidget extends Widget
{
    public $passedExam;

    /**
     * Marking answers for question depend user's answer
     *
     * @return string
     */
    public function run()
    {
        /* @var $passedQuestion \common\models\PassedQuestion */
        /* @var $question \common\models\Question */
        /* @var $answer \common\models\Answer */

        if (empty($this->passedExam)) {
            return 'Error: Wrong PassedExam!';
        }

        $str = '';
        $passedAnswers = [];
        foreach ($this->passedExam->passedQuestions as $passedQuestion) {
            $pssdAnswers = $passedQuestion->getPassedAnswers()->select(['answer_id', 'right'])->asArray()->all();
            $passedAnswers = array_merge($passedAnswers, $pssdAnswers);
        }
        $rightAnswers = ArrayHelper::map($passedAnswers, 'answer_id', 'right');

        $questionsContainerWidth = count($this->passedExam->exam->questions) * 100;
        $str .= '<ul class="questions-container clearfix" style="width: ' . $questionsContainerWidth . '%">';
        foreach ($this->passedExam->exam->questions as $question) {

            $point = 0;
            foreach ($this->passedExam->passedQuestions as $passedQuestion) {
                if ($passedQuestion->question_id === $question->id) {
                    $point = $passedQuestion->point;
                    break;
                }
            }

            $str .= '<li class="item question">';
            $str .= '    <p class="title">' . $question->title . '</p>';
            $str .= '    <p class="description">' . $question->description . '</p>';
            $str .= '    <p class="description">Earned points: ' . $point . '/' . $question->point .'</p>';
            $str .= '    <ul class="items answers">';

            foreach ($question->answers as $answer) {
                $checked = ArrayHelper::getValue($rightAnswers, $answer->id);
                $class = ($answer->right) ? ' answer-right' : ' answer-wrong';
                $class .= ($checked) ? ' passed-right' : '';
                $str .= '<li class="item answer clearfix ' . $class . '">' . $answer->description . '</li>';
            }

            $str .= '</ul></li>';
        }
        $str .= '</ul>';

        return $str;
    }
}