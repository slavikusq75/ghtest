<?php

namespace common\components\widgets;


use Yii;
use yii\base\Widget;

class UserNameWidget extends Widget
{
    public function run()
    {
        if (empty(Yii::$app->user->identity->firstname)) {
            $userName = empty(Yii::$app->user->identity->username) ? 'Guest' :
                Yii::$app->user->identity->username;
        } else {
            $userName = Yii::$app->user->identity->firstname;
            $userName .= empty(Yii::$app->user->identity->lastname) ? '' :
                ', ' . Yii::$app->user->identity->lastname;
        }

        return $userName;
    }
}