<?php
namespace frontend\models;

use common\components\validators\EmailUniqueValidator;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id;
    public $teacher_token;
    public $username;
    public $email;
    public $phone;
    public $firstname;
    public $middlename;
    public $lastname;
    public $password;
    public $passwordRepeat;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', EmailUniqueValidator::className()],

            [['phone', 'firstname', 'middlename', 'lastname'], 'filter', 'filter' => 'trim'],
            [['phone', 'firstname', 'middlename', 'lastname'], 'required'],
            ['phone', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This phone has already been taken.'],
            [['phone'], 'string', 'min' => 6],
            [['firstname', 'middlename', 'lastname'], 'string', 'max' => 255],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            [['passwordRepeat'], 'required'],
            [['passwordRepeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'The password does not match with password repeat.' ],

            [['teacher_token'], 'safe'],
        ];
    }

    /**
     * Validates the password match with passwordRepeat.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePasswordRepeat($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->password !== $this->passwordRepeat) {
                $this->addError($attribute, '');
            }
        }
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = (empty($this->teacher_token)) ? new User() : User::findByTeacherToken($this->teacher_token);
            $user->username = $this->username;
            $user->email = $this->email;
            $user->phone = $this->phone;
            $user->firstname = $this->firstname;
            $user->middlename = $this->middlename;
            $user->lastname = $this->lastname;
            $user->status = User::STATUS_ACTIVE;
            $user->setPassword($this->password);
            $user->generateAuthKey();

            if ($user->save(false)) {
                if (empty($user->teacher_token)) {
                    $auth = Yii::$app->authManager;
                    $authorRole = $auth->getRole('user');
                    $auth->assign($authorRole, $user->getId());
                } else {
                    $auth = Yii::$app->authManager;
                    $authorRole = $auth->getRole('teacher');
                    $auth->assign($authorRole, $user->getId());
                }
            }

            return $user;
        } else {
            if (!empty($user) && $user->hasErrors()) {
                foreach ($user->errors as $error) {
                    Yii::$app->session->setFlash('error', $error);
                }
            }
        }

        return null;
    }
}
