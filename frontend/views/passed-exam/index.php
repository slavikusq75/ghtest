<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $passedExams array */
/* @var $passedExam common\models\PassedExamSearch */

$this->title = 'Passed Exams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="passed-exam-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="items clearfix">
        <div class="items-wrap clearfix">
            <?php
            foreach ($passedExams as $passedExam) {
                ?>
                <div class="item exam">
                    <div
                        class="title"><?= Html::a(Html::encode($passedExam->exam->title), ['view', 'id' => $passedExam->id]) ?></div>
                    <div class="description">
                        <p><?= Html::encode($passedExam->exam->description) ?></p>
                        <p>Timer: <?= Html::encode($passedExam->timer) ?></p>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>