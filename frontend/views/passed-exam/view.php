<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\components\widgets\MarkPassedQuestionsAnswersWidget;

/* @var $this yii\web\View */
/* @var $model common\models\PassedExam */
/* @var $passedQuestion common\models\PassedQuestion */
/* @var $answer common\models\Answer */

$this->title = $model->exam->title;
$this->params['breadcrumbs'][] = ['label' => 'Passed Exams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="passed-exam-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p class="description"><?php echo $model->exam->description ?></p>
    <p>Timer:
        <span id="timerExam" class="timer"><?php echo $model->timer ?></span>
    </p>
    <p>Earned points:
        <span id="passedExamPoint"><?php echo $model->point ?></span>
    </p>

    <div class="item questions">
        <div class="question-current">
            <?php echo MarkPassedQuestionsAnswersWidget::widget(['passedExam' => $model]); ?>
        </div>
        <div class="control control-left"> &lt;--</div>
        <div class="control control-right"> --&gt; </div>
    </div>

</div>

