<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use frontend\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <title> SmartAdmin</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php $this->head() ?>
    <link rel="shortcut icon" href="/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/img/favicon/favicon.ico" type="image/x-icon">


    <link rel="apple-touch-icon" href="/img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/splash/touch-icon-ipad-retina.png">

    <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Startup image for web apps -->
    <link rel="apple-touch-startup-image" href="/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
    <link rel="apple-touch-startup-image" href="/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
    <link rel="apple-touch-startup-image" href="/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

</head>

<body class="animated fadeInDown">
<?php $this->beginBody() ?>
<header id="header">

    <div id="logo-group">
        <span id="logo"> <img src="/img/logo.png" alt="GeekHub"> </span>
    </div>

    <span id="extr-page-header-space">
    <?php
    if ($this->context->action->id == "login") {
        ?>
        <span class="hidden-mobile">Need an account?</span>
        <a href="<?=Url::to(['site/signup'])?>" class="btn btn-danger">Create account</a>
        <?php
    } else {
        ?>
        <span class="hidden-mobile">Has an account?</span>
        <a href="<?=Url::to(['site/login'])?>" class="btn btn-danger">Login</a>
        <?php
    }
    ?>
    </span>
</header>

<?= Alert::widget() ?>

<div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">
            <?=$content?>
    </div>

</div>

<!--================================================== -->
<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
