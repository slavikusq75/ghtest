<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
        <h1 class="txt-color-red login-header-big">GeekHub</h1>
        <div class="hero">

            <div class="pull-left login-desc-box-l">
                <h4 class="paragraph-header">Platform for test.</h4>
            </div>

            <img src="/img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
        <div class="well no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options'=>['class' => 'smart-form client-form']
            ]); ?>
                <header>Sign In</header>

                <fieldset>
                    <?= $form->field($model, 'username') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                </fieldset>
                <footer>
                    <?= Html::submitButton('Sign in', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </footer>

            <?php ActiveForm::end(); ?>

        </div>

        <h5 class="text-center"> - Or sign in using -</h5>
        <?= yii\authclient\widgets\AuthChoice::widget([
            'baseAuthUrl' => ['site/auth']
        ]) ?>

    </div>
</div>

