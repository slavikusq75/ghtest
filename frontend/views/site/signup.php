<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 hidden-xs hidden-sm">
        <h1 class="txt-color-red login-header-big">GeekHub</h1>

        <div class="hero">
            <div class="pull-left login-desc-box-l">
                <h4 class="paragraph-header">Platform for test.</h4>
            </div>
            <img src="/img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
        <div class="well no-padding">
            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'options' => ['class' => 'smart-form client-form']
            ]); ?>
            <header>
                Registration
            </header>

            <fieldset>
                <?php
                if (!empty($model->teacher_token)) {
                    echo $form->field($model, 'teacher_token')->hiddenInput()->label(false);
                }
                ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'phone') ?>
                <?= $form->field($model, 'firstname') ?>
                <?= $form->field($model, 'middlename') ?>
                <?= $form->field($model, 'lastname') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'passwordRepeat')->passwordInput() ?>
            </fieldset>
            <footer>
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </footer>

            <?php ActiveForm::end(); ?>
        </div>
        <h5 class="text-center">- Or sign in using -</h5>
        <?= yii\authclient\widgets\AuthChoice::widget([
            'baseAuthUrl' => ['site/auth']
        ]) ?>
    </div>
</div>
