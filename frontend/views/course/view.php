<?php

use common\models\Exam;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Course */
/* @var $exams array */
/* @var $exam common\models\Exam */
/* @var $questionDataProvider yii\data\ActiveDataProvider */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Courses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$titleQuestions = 'Questions';
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="course-detail-view">
        <div class="form-group field-course-season_id required">
            <label class="control-label" for="view-course-season_id">Season ID</label>
            <input type="text" id="view-course-season_id" class="form-control" name="season_id"
                   value="<?= $model->season->title ?>" readonly="readonly" maxlength="255">
        </div>
        <div class="form-group field-course-title required">
            <label class="control-label" for="view-course-title">Title</label>
            <input type="text" id="view-course-title" class="form-control" name="title"
                   value="<?= $model->title ?>" readonly="readonly" maxlength="255">
        </div>
        <div class="form-group field-course-description">
            <label class="control-label" for="view-course-description">Description</label>
            <input type="text" id="view-course-description" class="form-control" name="description"
                   value="<?= $model->description ?>" readonly="readonly" maxlength="255">
        </div>
    </div>


    <h1>Course Exams:</h1>
    <div class="items exams clearfix">
        <?php
        if (empty($exams)) {
            ?>
            <h1 class="alert">There no exams yet. Come back later.</h1>
            <?php
        }
        ?>

        <div class="items-wrap clearfix">
            <?php
            foreach ($exams as $exam) {
                ?>
                <div class="item exam">
                    <h1><?= $exam->title; ?></h1>
                    <p class="description"><?= $exam->description; ?></p>
                    <p class="description">This exam can be passed from <?= date('m/d/Y', $exam->date_begin) ?> to <?= date('m/d/Y', $exam->date_end) ?></p>
                    <?php
                    if (!Yii::$app->user->isGuest && $exam->course->isUserSigned(Yii::$app->user->identity->getId())) {
                        if ($exam->canBePassed()) {
                            ?>
                            <p>
                                <?= Html::a('Pass Test', ['exam/pass/' . $exam->id], ['class' => 'btn btn-success']) ?>
                            </p>
                            <?php
                        }
                    } else {
                        ?>
                        <p>
                            <?= Html::a('To pass exam you need to be signed in to that course', ['course/index']) ?>
                        </p>
                    <?php } ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

</div>
