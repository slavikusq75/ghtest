<?php

use common\models\Course;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $course common\models\Course */

$this->title = 'Courses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    $courses = $dataProvider->getModels();
    foreach ($courses as $course) {
        ?>
        <div class="course">
            <div class="title"><?= Html::a(Html::encode($course->title), ['course/view', 'id' => $course->id]) ?></div>
            <div class="description">
                <p><?= Html::encode($course->description) ?></p>
            </div>
            <div class="actions">
                <?php
                if ($course->isUserSigned(yii::$app->getUser()->getId())) {
                    ?>
                    <p>Already signed on course</p>
                    <?php
                } else {
                    echo Html::a('Sign Up on Course', ['signup', 'id' => $course->id], ['class' => 'btn btn-success']);
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
//    echo GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'title',
//            'description',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]);
    ?>

</div>
