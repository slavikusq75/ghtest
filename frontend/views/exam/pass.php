<?php

/* @var $passedExam common\models\PassedExam */
/* @var $exam common\models\exam */
/* @var $question common\models\Question */
/* @var $answer common\models\Answer */

use yii\helpers\Html;

$exam = $passedExam->exam;
$this->title = 'Pass Test ' . $exam->title;

?>
<div class="app">
    <div class="pass-test">
        <?= Html::input('hidden', 'passExamId', $passedExam->id) ?>
        <?= Html::input('hidden', 'examId', $exam->id) ?>
        <h1><?= Html::encode($this->title) ?></h1>
        <p class="description"><?= $exam->description ?></p>
        <p>Timer:
            <span id="timerExam" class="timer"><?= $exam->timer ?></span>
        </p>
        <div>
            <span class="start-exam btn btn-success">Start exam</span>
            <span class="finish-exam btn btn-success hidden">Finish exam</span>
        </div>

        <!--  questions  -->

        <div class="resultMessage hidden">
            <p class="message"></p>
        </div>

    </div>

</div>