<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 20.02.16
 * Time: 0:22
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class PassExamAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/angular/app.js',
        'js/angular/services/examtimertervice.js',
        'js/angular/controllers/passexamcontroller.js'
    ];
    public $depends = [
        'frontend\assets\AngularAsset',
    ];
}