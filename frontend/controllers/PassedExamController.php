<?php

namespace frontend\controllers;

use Yii;
use common\models\PassedExam;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PassedExamController implements the CRUD actions for PassedExam model.
 */
class PassedExamController extends Controller
{
    /**
     * Lists all PassedExam models.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        if (yii::$app->user->isGuest) {
            yii::$app->getSession()->setFlash('error', 'You must be logged in to sign up to this course!');
            $this->redirect(['site/login']);
        } else {
            $userID = Yii::$app->user->identity->getId();
            $passedExams = PassedExam::find()
                ->where(['user_id' => $userID])
//                 if timer is null then exam has not been passed
                ->andWhere(['is not', 'timer', null])
                ->all();

            return $this->render('index', [
                'passedExams' => $passedExams
            ]);
        }
    }

    /**
     * Displays a single PassedExam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the PassedExam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PassedExam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PassedExam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
