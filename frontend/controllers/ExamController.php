<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\models\Exam;
use common\models\Question;
use common\models\Answer;
use common\models\PassedExam;
use common\models\PassedQuestion;
use common\models\PassedAnswer;

/**
 * PassedExamController implements the CRUD actions for PassedExam model.
 */
class ExamController extends Controller
{

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPass($id)
    {

        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('error', 'You must be logged in.');
            $this->goBack();
        }
        if (!Yii::$app->user->can('indexExam')) {
            Yii::$app->session->setFlash('error', 'You do not have permission for this action.');
            return $this->goHome();
        }

        $exam = $this->findModel($id);

        if (!$exam->canBePassed()) {
            Yii::$app->session->setFlash('error', 'This exam can be passed from ' . date('m/d/Y', $exam->date_begin) .
                ' to ' . date('m/d/Y', $exam->date_end) . '!');
            $this->goBack();
        }

        $userId = Yii::$app->user->identity->getId();
//        prepare test
        $passedExam = PassedExam::findOne(['exam_id' => $exam->id, 'user_id' => $userId]);
        if (empty($passedExam)) {
            $passedExam = new PassedExam();
            $passedExam->user_id = $userId;
            $passedExam->exam_id = $exam->id;
            $passedExam->save();
        }

        return $this->render('pass', [
            'passedExam' => $passedExam,
        ]);
    }


    public function actionStart($id)
    {
        $userId = Yii::$app->user->identity->getId();
        $passedExam = PassedExam::findOne(['user_id' => $userId, 'exam_id' => $id]);

        $exam = Exam::find()->where(['id' => $id])->
            select(['id', 'course_id', 'title', 'description', 'timer'])->
            with(['questions' => function($q) {
                    $q->select(['id', 'exam_id', 'title', 'description', 'type', 'point'])->
                        with(['answers' => function($a) {
                            $a->select(['id', 'question_id', 'description']);
                        }]);
            }])->asArray()->all();

        $passedExam->started_at = time();
        $passedExam->save();

        \Yii::$app->response->format = 'json';
        return $exam;
    }

    /**
     * @param int $examId
     * @param int $questionId
     * @param int $answerId
     * @param int $right
     * @return string $json
     */
    public function actionAnswer($examId, $questionId, $answerId, $right = 1)
    {
        $message = '';
        $error = 0;

        $userId = Yii::$app->user->identity->getId();
        $exam = Exam::findOne($examId);
        $question = Question::findOne($questionId);
        $answer = Answer::findOne($answerId);
        $passedExam = PassedExam::findOne(['user_id' => $userId, 'exam_id' => $exam->id]);
        $passedQuestion = PassedQuestion::findOne(['passed_exam_id' => $passedExam->id, 'question_id' => $question->id]);
        if (!empty($passedQuestion)) {
            $passedAnswer = PassedAnswer::findOne(['passed_question_id' => $passedQuestion->id, 'answer_id' => $answer->id]);
        }

        if (empty($question) || empty($answer)) {
//            Yii::$app->session->setFlash('error', 'Unknown question!');
            $message = 'Unknown question!';
            $error = 1;
        } else {
            if (empty($passedQuestion)) {
                $passedQuestion = new PassedQuestion();
                $passedQuestion->passed_exam_id = $passedExam->id;
                $passedQuestion->question_id = $question->id;
                $passedQuestion->save();
            }

            if (empty($passedAnswer)) {
                $passedAnswer = new PassedAnswer();
                $passedAnswer->passed_question_id = $passedQuestion->id;
                $passedAnswer->answer_id = $answer->id;
            }
            $passedAnswer->right = $right;
            if ($passedAnswer->save()) {
                $message = 'Answer has been accepted!';
                $error = 0;
            } else {
                $message = 'Error occurred! Answer has not been saved!';
                $error = 1;
            }
        }

        $json = json_encode(['error' => $error, 'message' => $message]);
        return $json;
    }

    public function actionFinish($id, $timer)
    {
        $message = '';
        $error = 0;

        $userId = Yii::$app->user->identity->getId();
        $exam = Exam::findOne($id);
        $examId = $exam->id;
        $passedExam = PassedExam::findOne(['user_id' => $userId, 'exam_id' => $examId]);

        if (empty($passedExam)) {
            $error = 1;
            $message = 'Error: Wrong exam Id';
        } else {
            // calc exam result
            $resExam = $this->calcExamPoint($examId, $userId);

            $message = 'You earn ' . $resExam['pointExam'] . ' points.';

            //TODO: check if user's timer value is truly
            // something like
            $finisTime = time();
            if ($passedExam->started_at + $timer !== $finisTime) {
                // Error message
                // user passed Exam with incorrect value of timer
            }

            $passedExam->timer = $timer;
            $passedExam->finished_at = time();
            $passedExam->point = $resExam['pointExam'];
            $passedExam->save();
        }

        $res = ['error' => $error, 'message' => $message];
        if (!empty($resExam)) {
            $res['resExam'] = $resExam;
        }

        $json = json_encode($res);
        return $json;
    }

    /**
     * Calculating exam's result with saving point for questions
     * @param $examId integer
     * @param $userId integer
     * @return array ['pointExam', 'passedQuestions']
     */
    private function calcExamPoint($examId, $userId)
    {
        $pointExam = 0;
        $resPassedQuestions = [];

        $arrPassedExam = PassedExam::find()->where(['exam_id' => $examId, 'user_id' => $userId])
            ->select(['id', 'user_id', 'exam_id', 'timer'])
            ->with(['passedQuestions' => function($pq) {
                $pq->select(['id', 'passed_exam_id', 'question_id', 'point'])
                    ->with(['question' => function($q) {
                        $q->select(['id', 'title', 'point'])
                            ->with(['answers' => function($a) {
                                $a->select(['id', 'question_id', 'description', 'right']);
                            }]);
                    }])
                    ->with(['passedAnswers' => function($pa) {
                        $pa->select(['id', 'passed_question_id', 'answer_id', 'right']);
                    }]);
            }])->asArray()->all();

        if (!empty($arrPassedExam)) {
            foreach ($arrPassedExam[0]['passedQuestions'] as $arrPassedQuestion) {
                $questionId = $arrPassedQuestion['question_id'];
                foreach ($arrPassedQuestion['passedAnswers'] as $key => $arrPassedAnswer) {
                    $resPassedAnswers[$arrPassedAnswer['answer_id']] = $arrPassedAnswer['right'];
                }
                foreach ($arrPassedQuestion['question']['answers'] as $key => $answer) {
                    if ($answer['right']) {
                        $answerId = $answer['id'];
                        $passedRight = empty($resPassedAnswers[$answerId]) ? 0 : $resPassedAnswers[$answerId];
                        $resPassedQuestions[$questionId][$answerId] = (int)($answer['right'] == $passedRight);
                    }
                }
                $countAnswers = count($resPassedQuestions[$questionId]);
                $countAnswers = ($countAnswers == 0) ? 1 : $countAnswers;  // prevent division by zero
                $sumAnswers = array_sum($resPassedQuestions[$questionId]);
                $pointQuestion = $arrPassedQuestion['question']['point'];
                $point = round(($sumAnswers/$countAnswers)*$pointQuestion, 2);
                $resPassedQuestions[$questionId]['point'] = $point;
                $pointExam += $point;

                $passedQuestion = PassedQuestion::findOne($arrPassedQuestion['id']);
                $passedQuestion->point = $point;
                $passedQuestion->save();
            }
        }

        return ['pointExam' => $pointExam, 'passedQuestions' => $resPassedQuestions];
    }

    /**
     * Finds the Exam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PassedExam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Exam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the PassedExam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PassedExam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findPassedExam($id)
    {
        if (($model = PassedExam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
