<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 31.01.16
 * Time: 18:09
 */

namespace frontend\controllers;


use common\models\Course;
use common\models\QuestionSearch;
use common\models\UserCourse;
use Yii;
use yii\web\Controller;
use common\models\CourseSearch;
use yii\web\NotFoundHttpException;

class CourseController extends Controller
{

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $queryParams = Yii::$app->request->queryParams;
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $exams = $model->exams;

        $questionSearchModel = new QuestionSearch();
        $questionDataProvider = $questionSearchModel->search([]);

        return $this->render('view', [
            'model' => $model,
            'exams' => $exams,
            'questionDataProvider' => $questionDataProvider,
        ]);
    }

    /**
     * Signing up user to course
     * @param $id integer course id
     */
    public function actionSignup($id)
    {
        if (yii::$app->user->isGuest) {
            yii::$app->getSession()->setFlash('error', 'You must be logged in to sign up to this course!');
        } else {
            $userCourse = new UserCourse();
            $userCourse->user_id = yii::$app->user->identity->getId();
            $userCourse->course_id = $id;
            if ($userCourse->save()) {
                // assign role student if no assigned yet
                $this->assignRole('student', $userCourse->user_id);
                yii::$app->getSession()->setFlash('success', 'You has been signed up to this course successfully!');
            } else {
                yii::$app->getSession()->setFlash('error', 'Error is occurred on signed up to this course! Try again.');
            }
        }
        $this->redirect(['course/index']);
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Assign role to user if role no assigned yet
     * @param $roleName string
     * @param $userId integer
     */
    protected function assignRole($roleName, $userId)
    {
        $auth = yii::$app->authManager;
        $userRoles = $auth->getRolesByUser($userId);
        if (!array_key_exists($roleName, $userRoles)) {
            $role = $auth->getRole($roleName);
            $auth->assign($role, $userId);
        }
    }

}