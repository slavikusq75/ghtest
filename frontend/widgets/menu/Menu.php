<?php

namespace frontend\widgets\menu;

use yii\base\Exception;
use yii\bootstrap\Widget;
use yii\helpers\Url;
use yii\web\View;

/**
 *
 * @author NmDimas  <nmdimas@mail.ru>
 */
class Menu extends Widget
{
    /**
     * @var array Menu Items
     */
    public $items = [];
    /** @var string */
    private $actionCurrentName;
    /** @var string */
    private $controllerCurrentName;

    public $baseUrl = '/admin';
    /** @var  View */
    public $view;
    public $inoutHTML = '';

    public function init()
    {
        parent::init();
        if (!($this->view instanceof \yii\base\View)) {
            throw new Exception('empty View object in menu widget');
        }
        $this->actionCurrentName = $this->view->context->action->id;
        $this->controllerCurrentName = $this->view->context->id;
    }

    public function run()
    {
        foreach ($this->items as $item) {
            if (array_key_exists('title', $item)) {
                $this->renderNestedItem($item);
            } else {
                $this->renderItem($item);
            }
        }

        return $this->inoutHTML;
    }

    public function renderItem($item)
    {
        if (empty($item['name']) or empty($item['action']) or empty($item['controller'])) {
            throw new Exception('don\'t correct item in menu');
        }
        if (!empty($item['items'])) {
            $this->renderItem($item['items']);
        }
        $this->inoutHTML .= $this->render('menu', [
            'url' => $this->createUrl($item),
            'item' => $item,
            'controllerCurrentName' => $this->controllerCurrentName,
            'actionCurrentName' => $this->actionCurrentName
        ]);
    }

    public function renderNestedItem($items)
    {
        $this->inoutHTML .= '<li><a href="#"><i class="fa fa-lg fa-fw fa fa-book"></i> <span class="menu-item-parent">' . $items['title'] . '</span></a><ul>';

        foreach ($items['items'] as $item) {
            $this->inoutHTML .= $this->render('menu', [
                'url' => $this->createUrl($item),
                'item' => $item,
                'controllerCurrentName' => $this->controllerCurrentName,
                'actionCurrentName' => $this->actionCurrentName
            ]);
        }
        $this->inoutHTML .= '</ul></li>';
    }

    protected function createUrl($item)
    {
        $url = empty($item['module']) ? '' : '/'.$item['module'];
        $url .= $item['controller'].'/'.$item['action'];

        return Url::to($this->baseUrl.'/'.$url, true);
    }
}
