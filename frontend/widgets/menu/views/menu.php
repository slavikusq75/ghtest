<?php
/**
 * @var array $item
 * @var string $url
 * @var string $actionCurrentName
 * @var string $controllerCurrentName
 */
$icon = (empty($item['icon'])) ? 'fa-file' :  $item['icon'];
?>

<li class="<?php if ($item['controller'] == $controllerCurrentName && $item['action'] == $actionCurrentName) {
    echo 'active';
} ?>">
    <a href=" <?= $url ?>"><i class="fa <?=$icon?>"></i>
        <span class="menu-item-parent"> <?= $item['name']; ?> </span>
        <span class="badge pull-right bg-color-redLight inbox-badge"><?= (isset($item['notification_count']) && $item['notification_count'] != 0) ? $item['notification_count'] : '' ?></span>
    </a>
</li>
