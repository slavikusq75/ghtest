<?php



namespace backend\widgets\flashAlerts;

use yii\base\Widget;

class FlashAlerts extends Widget
{
    public function run()
    {
        if (\Yii::$app->session->hasFlash('success')) {
            foreach (\Yii::$app->session->getFlash('success') as $message) {
                $this->view->registerJs('$.smallBox({
                title : "Success",
                content : "'.$message.'",
                color : "#739E73",
                iconSmall : "fa fa-cloud",
                timeout : 5000});');
            }
        } elseif (\Yii::$app->session->hasFlash('error')) {
            foreach (\Yii::$app->session->getFlash('error') as $message) {
                $this->view->registerJs('$.smallBox({
                title : "Error",
                content : "'.$message.'",
                color : "#c70000",
                iconSmall : "fa fa-cloud",
                timeout : 5000});');
            }
        }
    }
}
