;(function(global, $) {

    var examTimer = {
        $timer: null,
        timer: null,
        timerVal: 0,
        stopped: 0,
        callback: null,

        stop: function(){
            if (!this.stopped) {
                clearInterval(this.timer);
                this.stopped = 1;
                if ($.isFunction(this.callback)) {
                    this.callback();
                }
            }
        },
        run: function($divTimer, callback){
            this.$timer = $divTimer;
            this.callback = callback;
            this.timerVal = parseInt(this.$timer.text());
            this.timer = setInterval(this.schedule, 1000);
        },
        schedule: function() {
            if (examTimer.timerVal > 0) {
                examTimer.timerVal--;
                examTimer.$timer.text(examTimer.timerVal);
            } else {
                examTimer.stop();
            }
        }
    };

    var templateAnswer = '<li class="item answer clearfix" > ' +
        '   <input type="hidden" name="passedAnswerId" value=""> ' +
        '   <input type="hidden" name="answerId" value="%%answerId%%"> ' +
        '   <label class="checkbox"> ' +
        '       <input type="checkbox" name="answerIds[%%answerId%%]" > ' +
        '       <span class="">%%answerDescription%%</span> ' +
        '   </label> ' +
        '</li>';
    var templateQuestion = '<li class="item question" > ' +
        '   <input type="hidden" name="passedQuestionId" value=""> ' +
        '   <input type="hidden" name="questionId" value="%%questionId%%"> ' +
        '   <input type="hidden" name="questionType" value="%%questionType%%"> ' +
        '   <p class="description">%%questionDescription%%</p> ' +
        '   <ul class="item answers"> ' +
        '                        %%divAnswers%% ' +
        '   </ul> ' +
        '</li>';

    var templateQuestions = '<div class="item questions"> ' +
        '   <div class="question-current"> ' +
        '       <ul class="questions-container clearfix" > ' +
        '                %%divQuestion%% ' +
        '       </ul> ' +
        '   </div> ' +
        '   <div class="control control-left" > &lt;--</div> ' +
        '   <div class="control control-right" > --&gt; </div> ' +
        '</div>';

    function getDivFromTemplate(template, params)
    {
        var pattern = '';
        var div = template;
        if ($.isArray(params) || $.isPlainObject(params)) {
            $.each(params, function(key, value){
                pattern = new RegExp('%%' + key + '%%', 'gi');
                div = div.replace(pattern, value);
            });
        }
        return div;
    }

    var finishingExam = function(){
        alert('Exam finished');
        var examId = parseInt($('.pass-test input[name=examId]').val());
        $('.pass-test .finish-exam').replaceWith('');
        $('.questions').replaceWith('');
        var timerVal = examTimer.timerVal;
        var url = '/exam/finish/' + examId + '/' + timerVal;
        $.get(url).done(function(response) {
            var data = JSON.parse(response);
            if (data.error) {
                alert(data.message);
            } else {
                alert(data.message);
                $('.resultMessage .message').text(data.message);
                $('.resultMessage').removeClass('hidden');
            }
        }).fail(function() {
                alert("Error PassExamController.finishingExam");
        });
    };

    // start exam
    $('.pass-test .start-exam').click(function(){
        // get questions
        var $timer = $('.pass-test .timer');
        examTimer.run($timer, finishingExam);
        $(this).replaceWith('');
        $('.pass-test .finish-exam').toggleClass('hidden');
        //$('.pass-test .questions').toggleClass('hidden show');

        var passedExamId = $('.pass-test input[name=examId]').val();

        $.getJSON('/exam/start/' + passedExamId, function(response){
            if (response.error) {
                alert(response.message);
            } else {
                var divQuestions ='';
                var divQuestion ='';
                var exam = response[0];
                $.each(exam.questions, function(keyQ, valueQ){
                    var divAnswers = '';
                    $.each(valueQ.answers, function(keyA, valueA){
                        var paramsAnswer = {
                            passedAnswerId      : '',
                            answerId            : valueA.id,
                            answerDescription   : valueA.description,
                            answerChecked       : ''
                        };
                        divAnswers += getDivFromTemplate(templateAnswer, paramsAnswer);
                    });

                    var paramsQuestion = {
                        questionId          :   valueQ.id,
                        questionType        :   valueQ.type,
                        questionTypeMessage :   valueQ.type,
                        questionDescription :   valueQ.description,
                        divAnswers           :   divAnswers
                    };
                    divQuestion += getDivFromTemplate(templateQuestion, paramsQuestion);
                });
                divQuestions += getDivFromTemplate(templateQuestions, {divQuestion: divQuestion});
                $('.pass-test').append(divQuestions);
                var width = exam.questions.length * 100;
                $('.pass-test .question-current .questions-container').css({'width' : width + '%'});

            }
        });

    });

    // finish exam
    $('.pass-test .finish-exam').click(function(){
        examTimer.stop();
        alert('Your time: ' + examTimer.timerVal);
        $(this).replaceWith('');
    });

    // next/prev question
    $(document).on('click', '.control', function(){
        var $questionsContainer = $('.question-current .questions-container');
        var marginLeft = parseInt($questionsContainer.css('margin-left'));
        var widthQuestionContainer = parseInt($questionsContainer.css('width'));
        var widthQuestionCurrent = parseInt($('.question-current').css('width'));
        if ($(this).hasClass('control-left')) {
            if (marginLeft >= 0) {
                marginLeft = (-1) * (widthQuestionContainer - widthQuestionCurrent);
            } else {
                marginLeft += widthQuestionCurrent;
            }
        } else {
            if (widthQuestionContainer - widthQuestionCurrent + marginLeft <= 0) {
                marginLeft = 0;
            } else {
                marginLeft = marginLeft - widthQuestionCurrent;
            }
        }
        $questionsContainer.css('margin-left', marginLeft);
    });

    $(document).on('click', '.answer', function(){
        var $answer = $(this);
        var $question = $answer.parents('.question');
        var $exam = $question.parents('.pass-test');
        var examId = $exam.find('input[name=examId]').val();
        var questionId = $question.find('input[name=questionId]').val();
        var answerId = $answer.find('input[name=answerId]').val();
        var answerChecked = $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked');
        answerChecked = answerChecked ? answerChecked : false;

        $.getJSON('/exam/answer', {
            examId      : examId,
            questionId  : questionId,
            answerId    : answerId,
            right       : answerChecked ? 0 : 1
        }, function(response){
            if (response.error) {
                alert(response.message);
            } else {
                if (answerChecked) {
                    $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', false);
                    $answer.removeClass('checked');
                } else {
                    $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', true);
                    $answer.addClass('checked');
                }
            }
        });

    });

})(this, jQuery);
