App.controller('PassExamController', ['$scope', '$http', 'examTimer',
    function($scope, $http, examTimer) {

        $scope.startExam = function($event) {

            var timerElement = angular.element('#timerExam');
            $scope.timerVal = timerElement.text();

            var examId = angular.element('.pass-test input[name=examId]').val();
            var url = '/exam/start/' + examId;
            $http.get(url).then(
                function (response) {  //successCallback
                    if (response.status != 200) {
                        alert(response.message);
                    } else {
                        $scope.questions = response.data[0].questions;

                        if ($scope.questions) {
                            var countQuestions = $scope.questions.length;
                            angular.element('.question-current .questions-container').css('width', (countQuestions * 100) + '%');

                            console.log('countQuestions: ' + countQuestions);

                            examTimer.run(timerElement, finishingExam);
                            $scope.examStarted = 1;
                            angular.element($event.currentTarget).replaceWith('');
                        }
                    }
                },
                function () {  //errorCallback
                    alert("Error PassExamController.startExam");
                }
            );
        };

        $scope.finishExam = function($event){
            examTimer.stop();
        };

        finishingExam = function(){
            alert('Exam finished');
            var examId = parseInt(angular.element('.pass-test input[name=examId]').val());
            angular.element('.pass-test .finish-exam').replaceWith('');
            angular.element('.questions').replaceWith('');
            var timerVal = examTimer._value;
            var url = '/exam/finish/' + examId + '/' + timerVal;
            $http.get(url).then(
                function(response) {
                    if (response.error) {
                        alert(response.message);
                    } else {
                        alert(response.data.message);
                        $scope.resultMessage = response.data.message;
                    }
                },
                function() {
                    alert("Error PassExamController.finishingExam");
                }
            );
        };

        $scope.nextQuestion = function($event){
            if ($scope.questions) {
                var $questionsContainer = angular.element('.question-current .questions-container');
                var marginLeft = parseInt($questionsContainer.css('margin-left'));
                var widthQuestionContainer = parseInt($questionsContainer.css('width'));
                var widthQuestionCurrent = parseInt(angular.element('.question-current').css('width'));

                if (angular.element($event.currentTarget).hasClass('control-left')) {
                    if (marginLeft >= 0) {
                        marginLeft = (-1) * (widthQuestionContainer - widthQuestionCurrent);
                    } else {
                        marginLeft += widthQuestionCurrent;
                    }
                } else {
                    if (widthQuestionContainer - widthQuestionCurrent + marginLeft <= 0) {
                        marginLeft = 0;
                    } else {
                        marginLeft = marginLeft - widthQuestionCurrent;
                    }
                }
                $questionsContainer.css('margin-left', marginLeft);
            }
        };

        $scope.giveAnswer = function($event){
            var $answer = angular.element($event.currentTarget).parents('.answer');
            var $question = $answer.parents('.question');
            var $exam = $question.parents('.pass-test');
            var examId = $exam.find('input[name=examId]').val();
            var questionId = $question.find('input[name=questionId]').val();
            var answerId = $answer.find('input[name=answerId]').val();
            var answerChecked = $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked');
            answerChecked = answerChecked ? answerChecked : false;
            console.log('examId: ' + examId);
            console.log('questionId: ' + questionId);
            console.log('answerId: ' + answerId);
            console.log('answerChecked: ' + answerChecked);

            var url = '/exam/answer/' +  examId + '/' + questionId + '/' + answerId + '/' + (answerChecked ? 0 : 1);
            $http.get(url).then(
                function(response){
                    if (response.error) {
                        alert(response.message);
                        if (!answerChecked) {
                            $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', false);
                            $answer.removeClass('checked');
                        } else {
                            $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', true);
                            $answer.addClass('checked');
                        }
                    } else {
                        if (answerChecked) {
                            $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', false);
                            $answer.removeClass('checked');
                        } else {
                            $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', true);
                            $answer.addClass('checked');
                        }
                    }
                },
                function () {  //errorCallback
                    alert("Error PassExamController.giveAnswer");
                    if (!answerChecked) {
                        $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', false);
                        $answer.removeClass('checked');
                    } else {
                        $answer.find('input[name="answerIds[' + answerId + ']"]').attr('checked', true);
                        $answer.addClass('checked');
                    }
                }
            );
        }
    }
]);
