App.factory('examTimer', [
    function() {
        var examTimer = this;

        examTimer._element = {};
        examTimer._value = 0;
        examTimer.stopped = 0;
        examTimer.callback = null;
        examTimer.timer = 0;

        examTimer.run = function(timerElement, callback) {
            this._element = timerElement;
            this.callback = callback;
            this._value = parseInt(this._element.text());
            this.timer = setInterval(this.schedule, 1000);
        };

        examTimer.schedule = function() {
            if (examTimer._value > 0) {
                examTimer._value--;
                examTimer._element.text(examTimer._value);
            } else {
                examTimer.stop();
            }
        }

        examTimer.stop = function(){
            if (!this.stopped) {
                clearInterval(this.timer);
                this.stopped = 1;
                alert('Time out');
                if (angular.isFunction(this.callback)) {
                    this.callback();
                }
            }
        }

        return examTimer;
    }
]);