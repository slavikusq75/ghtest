<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_121548_create_passed_exam extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%passed_exam}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'exam_id' => $this->integer()->notNull(),
            'timer' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-passed_exam-user_id', '{{%passed_exam}}', 'user_id');
        $this->createIndex('idx-passed_exam-exam_id', '{{%passed_exam}}', 'exam_id');

        $this->addForeignKey('fk-passed_exam-user-user_id',
            '{{%passed_exam}}', 'user_id',
            '{{%user}}', 'id',
            'NO ACTION', 'NO ACTION'
        );
        $this->addForeignKey('fk-passed_exam-exam-exam_id',
            '{{%passed_exam}}', 'exam_id',
            '{{%exam}}', 'id',
            'NO ACTION', 'NO ACTION'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-passed_exam-user-user_id', '{{%passed_exam}}');
        $this->dropForeignKey('fk-passed_exam-exam-exam_id', '{{%passed_exam}}');

        $this->dropIndex('idx-passed_exam-user_id', '{{%passed_exam}}');
        $this->dropIndex('idx-passed_exam-exam_id', '{{%passed_exam}}');

        $this->dropTable('{{%passed_exam}}');
    }
}
