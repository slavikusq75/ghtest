<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_124306_create_passed_answer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%passed_answer}}', [
            'id' => $this->primaryKey(),
            'passed_question_id' => $this->integer()->notNull(),
            'answer_id' => $this->integer()->notNull(),
            'right' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-passed_answer-passed_question_id', '{{%passed_answer}}', 'passed_question_id');
        $this->createIndex('idx-passed_answer-answer_id', '{{%passed_answer}}', 'answer_id');

        $this->addForeignKey('fk-passed_answer-passed_question-passed_question_id',
            '{{%passed_answer}}', 'passed_question_id',
            '{{%passed_question}}', 'id',
            'NO ACTION', 'NO ACTION'
        );
        $this->addForeignKey('fk-passed_answer-answer-answer_id',
            '{{%passed_answer}}', 'answer_id',
            '{{%answer}}', 'id',
            'NO ACTION', 'NO ACTION'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-passed_answer-passed_question-passed_question_id', '{{%passed_answer}}');
        $this->dropForeignKey('fk-passed_answer-answer-answer_id', '{{%passed_answer}}');

        $this->dropIndex('idx-passed_answer-passed_question_id', '{{%passed_answer}}');
        $this->dropIndex('idx-passed_answer-answer_id', '{{%passed_answer}}');

        $this->dropTable('{{%passed_answer}}');
    }
}
