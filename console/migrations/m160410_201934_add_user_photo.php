<?php

use yii\db\Migration;

class m160410_201934_add_user_photo extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'photo', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'photo');
    }
}
