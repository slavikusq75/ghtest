<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_114136_create_exam extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%exam}}', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->string(),
            'timer' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-exam-course_id', '{{%exam}}', 'course_id');

        $this->addForeignKey('fk-exam-course-course_id',
            '{{%exam}}', 'course_id',
            '{{%course}}', 'id',
            'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('fk-exam-course-course_id', '{{%exam}}');

        $this->dropIndex('idx-exam-course_id', '{{%exam}}');

        $this->dropTable('{{%exam}}');
    }
}
