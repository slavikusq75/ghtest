<?php

use yii\db\Migration;

class m160409_153924_exam_add_start_end_date extends Migration
{
    public function up()
    {
        $this->addColumn('{{%exam}}', 'date_begin', $this->integer()->notNull());
        $this->addColumn('{{%exam}}', 'date_end', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%exam}}', 'date_begin');
        $this->dropColumn('{{%exam}}', 'date_end');
    }
}
