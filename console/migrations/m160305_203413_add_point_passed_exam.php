<?php

use yii\db\Migration;

class m160305_203413_add_point_passed_exam extends Migration
{
    public function up()
    {
        $this->addColumn('{{%passed_exam}}', 'point', $this->double());
        $this->addColumn('{{%passed_exam}}', 'started_at', $this->integer());
        $this->addColumn('{{%passed_exam}}', 'finished_at', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%passed_exam}}', 'point');
        $this->dropColumn('{{%passed_exam}}', 'started_at');
        $this->dropColumn('{{%passed_exam}}', 'finished_at');
    }
}
