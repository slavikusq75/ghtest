<?php

use yii\db\Migration;

class m160406_175910_add_teacher_token_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'teacher_token', $this->string()->unique());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'teacher_token');
    }
}


