<?php

use yii\db\Schema;
use yii\db\Migration;

class m160124_183910_add_year_season extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%season}}', 'status', $this->smallInteger()->notNull()->defaultValue(1));
        $this->addColumn('{{%season}}', 'year_begin', $this->integer()->notNull());
        $this->addColumn('{{%season}}', 'year_end', $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn('{{%season}}', 'year_begin');
        $this->dropColumn('{{%season}}', 'year_end');
    }
}
