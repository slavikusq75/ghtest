<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_191611_add_relation_course_season extends Migration
{
    public function up()
    {
        $this->createIndex('idx-course-season_id', '{{%course}}', 'season_id');

        $this->addForeignKey('fk-course-season-season_id',
            '{{%course}}', 'season_id',
            '{{%season}}', 'id',
            'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('fk-course-season-season_id', '{{%course}}');

        $this->dropIndex('idx-course-season_id', '{{%course}}');
    }
}
