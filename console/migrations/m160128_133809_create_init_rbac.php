<?php
use common\models\User;
use yii\db\Schema;
use yii\db\Migration;
class m160128_133809_create_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;
        // add "indexCourse" permission
        $indexCourse = $auth->createPermission('indexCourse');
        $indexCourse->description = 'Index Course';
        $auth->add($indexCourse);
        // add "viewCourse" permission
        $viewCourse = $auth->createPermission('viewCourse');
        $viewCourse->description = 'View Course';
        $auth->add($viewCourse);
        // add "createCourse" permission
        $createCourse = $auth->createPermission('createCourse');
        $createCourse->description = 'Create Course';
        $auth->add($createCourse);
        // add "updateCourse" permission
        $updateCourse = $auth->createPermission('updateCourse');
        $updateCourse->description = 'Update Course';
        $auth->add($updateCourse);
        // add "deleteCourse" permission
        $deleteCourse = $auth->createPermission('deleteCourse');
        $deleteCourse->description = 'Delete Course';
        $auth->add($deleteCourse);


        // add "indexSeason" permission
        $indexSeason = $auth->createPermission('indexSeason');
        $indexSeason->description = 'Index Season';
        $auth->add($indexSeason);
        // add "viewSeason" permission
        $viewSeason = $auth->createPermission('viewSeason');
        $viewSeason->description = 'View Season';
        $auth->add($viewSeason);
        // add "createSeason" permission
        $createSeason = $auth->createPermission('createSeason');
        $createSeason->description = 'Create Season';
        $auth->add($createSeason);
        // add "updateSeason" permission
        $updateSeason = $auth->createPermission('updateSeason');
        $updateSeason->description = 'Update Season';
        $auth->add($updateSeason);
        // add "deleteSeason" permission
        $deleteSeason = $auth->createPermission('deleteSeason');
        $deleteSeason->description = 'Delete Season';
        $auth->add($deleteSeason);


        // add "indexExam" permission
        $indexExam = $auth->createPermission('indexExam');
        $indexExam->description = 'Index Exam';
        $auth->add($indexExam);
        // add "viewExam" permission
        $viewExam = $auth->createPermission('viewExam');
        $viewExam->description = 'View Exam';
        $auth->add($viewExam);
        // add "createExam" permission
        $createExam = $auth->createPermission('createExam');
        $createExam->description = 'Create Exam';
        $auth->add($createExam);
        // add "updateExam" permission
        $updateExam = $auth->createPermission('updateExam');
        $updateExam->description = 'Update Exam';
        $auth->add($updateExam);
        // add "deleteExam" permission
        $deleteExam = $auth->createPermission('deleteExam');
        $deleteExam->description = 'Delete Exam';
        $auth->add($deleteExam);


        // add "indexAnswer" permission
        $indexAnswer = $auth->createPermission('indexAnswer');
        $indexAnswer->description = 'Index Answer';
        $auth->add($indexAnswer);
        // add "viewAnswer" permission
        $viewAnswer = $auth->createPermission('viewAnswer');
        $viewAnswer->description = 'View Answer';
        $auth->add($viewAnswer);
        // add "createAnswer" permission
        $createAnswer = $auth->createPermission('createAnswer');
        $createAnswer->description = 'Create Answer';
        $auth->add($createAnswer);
        // add "updateAnswer" permission
        $updateAnswer = $auth->createPermission('updateAnswer');
        $updateAnswer->description = 'Update Answer';
        $auth->add($updateAnswer);
        // add "deleteAnswer" permission
        $deleteAnswer = $auth->createPermission('deleteAnswer');
        $deleteAnswer->description = 'Delete Answer';
        $auth->add($deleteAnswer);


        // add "indexQuestion" permission
        $indexQuestion = $auth->createPermission('indexQuestion');
        $indexQuestion->description = 'Index Question';
        $auth->add($indexQuestion);
        // add "viewQuestion" permission
        $viewQuestion = $auth->createPermission('viewQuestion');
        $viewQuestion->description = 'View Question';
        $auth->add($viewQuestion);
        // add "createQuestion" permission
        $createQuestion = $auth->createPermission('createQuestion');
        $createQuestion->description = 'Create Question';
        $auth->add($createQuestion);
        // add "updateQuestion" permission
        $updateQuestion = $auth->createPermission('updateQuestion');
        $updateQuestion->description = 'Update Question';
        $auth->add($updateQuestion);
        // add "deleteQuestion" permission
        $deleteQuestion = $auth->createPermission('deleteQuestion');
        $deleteQuestion->description = 'Delete Question';
        $auth->add($deleteQuestion);


        // add "indexUser" permission
        $indexUser = $auth->createPermission('indexUser');
        $indexUser->description = 'Index User';
        $auth->add($indexUser);
        // add "viewUser" permission
        $viewUser = $auth->createPermission('viewUser');
        $viewUser->description = 'View User';
        $auth->add($viewUser);
        // add "createUser" permission
        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create User';
        $auth->add($createUser);
        // add "updateUser" permission
        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update User';
        $auth->add($updateUser);
        // add "deleteUser" permission
        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete User';
        $auth->add($deleteUser);


        // add "indexPassedAnswer" permission
        $indexPassedAnswer = $auth->createPermission('indexPassedAnswer');
        $indexPassedAnswer->description = 'Index PassedAnswer';
        $auth->add($indexPassedAnswer);
        // add "viewPassedAnswer" permission
        $viewPassedAnswer = $auth->createPermission('viewPassedAnswer');
        $viewPassedAnswer->description = 'View PassedAnswer';
        $auth->add($viewPassedAnswer);


        // add "indexPassedExam" permission
        $indexPassedExam = $auth->createPermission('indexPassedExam');
        $indexPassedExam->description = 'Index PassedExam';
        $auth->add($indexPassedExam);
        // add "viewPassedExam" permission
        $viewPassedExam = $auth->createPermission('viewPassedExam');
        $viewPassedExam->description = 'View PassedExam';
        $auth->add($viewPassedExam);


        // add "indexPassedQuestion" permission
        $indexPassedQuestion = $auth->createPermission('indexPassedQuestion');
        $indexPassedQuestion->description = 'Index PassedQuestion';
        $auth->add($indexPassedQuestion);
        // add "viewPassedQuestion" permission
        $viewPassedQuestion = $auth->createPermission('viewPassedQuestion');
        $viewPassedQuestion->description = 'View PassedQuestion';
        $auth->add($viewPassedQuestion);


        // add "user" role and give this role the "indexCourse", "viewCourse",
        // "indexSeason", "viewSeason" permissions
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $indexCourse);
        $auth->addChild($user, $viewCourse);
        $auth->addChild($user, $indexSeason);
        $auth->addChild($user, $viewSeason);
        // add "student" role role and give this role the "indexExam", "viewExam" permissions
        // as well as the permissions of the "user" role
        $student = $auth->createRole('student');
        $auth->add($student);
        $auth->addChild($student, $indexExam);
        $auth->addChild($student, $viewExam);
        $auth->addChild($student, $user);
        // add "teacher" role and give this role additional permissions
        // as well as the permissions of the "student" role
        $teacher = $auth->createRole('teacher');
        $auth->add($teacher);
        $auth->addChild($teacher, $indexAnswer);
        $auth->addChild($teacher, $indexQuestion);
        $auth->addChild($teacher, $indexPassedAnswer);
        $auth->addChild($teacher, $indexPassedExam);
        $auth->addChild($teacher, $indexPassedQuestion);
        $auth->addChild($teacher, $createCourse);
        $auth->addChild($teacher, $createAnswer);
        $auth->addChild($teacher, $createExam);
        $auth->addChild($teacher, $createQuestion);
        $auth->addChild($teacher, $updateCourse);
        $auth->addChild($teacher, $updateAnswer);
        $auth->addChild($teacher, $updateExam);
        $auth->addChild($teacher, $updateQuestion);
        $auth->addChild($teacher, $deleteCourse);
        $auth->addChild($teacher, $deleteAnswer);
        $auth->addChild($teacher, $deleteExam);
        $auth->addChild($teacher, $deleteQuestion);
        $auth->addChild($teacher, $viewCourse);
        $auth->addChild($teacher, $viewAnswer);
        $auth->addChild($teacher, $viewExam);
        $auth->addChild($teacher, $viewQuestion);
        $auth->addChild($teacher, $viewPassedAnswer);
        $auth->addChild($teacher, $viewPassedExam);
        $auth->addChild($teacher, $viewPassedQuestion);
        $auth->addChild($teacher, $student);
        // add "admin" role and give this role additional permissions
        // as well as the permissions of the "teacher" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createSeason);
        $auth->addChild($admin, $updateSeason);
        $auth->addChild($admin, $deleteSeason);
        $auth->addChild($admin, $indexUser);
        $auth->addChild($admin, $viewUser);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
        $auth->addChild($admin, $teacher);

        // Assign admin role to first users.
        $userId = User::find()->min('id');
        if (!empty($userId)) {
            $auth->assign($admin, $userId);
        }
    }
    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}