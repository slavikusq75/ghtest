<?php

use yii\db\Migration;

class m160305_211919_alter_point_passed_question extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%passed_question}}', 'point', $this->double());
    }

    public function down()
    {
        $this->alterColumn('{{%passed_question}}', 'point', $this->integer());
    }
}
