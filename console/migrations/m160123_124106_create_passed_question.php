<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_124106_create_passed_question extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%passed_question}}', [
            'id' => $this->primaryKey(),
            'passed_exam_id' => $this->integer()->notNull(),
            'question_id' => $this->integer()->notNull(),
            'point' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-passed_question-passed_exam_id', '{{%passed_question}}', 'passed_exam_id');
        $this->createIndex('idx-passed_question-question_id', '{{%passed_question}}', 'question_id');

        $this->addForeignKey('fk-passed_question-passed_exam-passed_exam_id',
            '{{%passed_question}}', 'passed_exam_id',
            '{{%passed_exam}}', 'id',
            'NO ACTION', 'NO ACTION'
        );
        $this->addForeignKey('fk-passed_question-question-question_id',
            '{{%passed_question}}', 'question_id',
            '{{%question}}', 'id',
            'NO ACTION', 'NO ACTION'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-passed_question-passed_exam-passed_exam_id', '{{%passed_question}}');
        $this->dropForeignKey('fk-passed_question-question-question_id', '{{%passed_question}}');

        $this->dropIndex('idx-passed_question-passed_exam_id', '{{%passed_question}}');
        $this->dropIndex('idx-passed_question-question_id', '{{%passed_question}}');

        $this->dropTable('{{%passed_question}}');
    }
}
