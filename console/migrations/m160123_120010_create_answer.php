<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_120010_create_answer extends Migration
{
    protected $tableName = '{{%answer}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer()->notNull(),
            'description' => $this->string()->notNull(),
            'right' => $this->smallInteger()->notNull(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-answer-question_id', $this->tableName, 'question_id');

        $this->addForeignKey('fk-answer-question-question_id',
            $this->tableName, 'question_id',
            '{{%question}}', 'id',
            'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('fk-answer-question-question_id', $this->tableName);

        $this->dropIndex('idx-answer-question_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
