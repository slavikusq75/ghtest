<?php

use common\models\Course;
use common\models\Season;
use yii\db\Schema;
use yii\db\Migration;

class m160125_223132_set_default_status_1 extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%season}}', 'status', $this->smallInteger()->notNull()->defaultValue(1));
        $this->alterColumn('{{%course}}', 'status', $this->smallInteger()->notNull()->defaultValue(1));
        Season::updateAll(['status' => 1]);
        Course::updateAll(['status' => 1]);
    }

    public function down()
    {
        return true;
    }
}
