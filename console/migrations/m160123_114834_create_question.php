<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_114834_create_question extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'exam_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'description' => $this->string(),
            'type' => $this->integer(),
            'point' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-question-exam_id', '{{%question}}', 'exam_id');

        $this->addForeignKey('fk-question-exam-exam_id',
            '{{%question}}', 'exam_id',
            '{{%exam}}', 'id',
            'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('fk-question-exam-exam_id', '{{%question}}');

        $this->dropIndex('idx-question-exam_id', '{{%question}}');

        $this->dropTable('{{%question}}');
    }
}
