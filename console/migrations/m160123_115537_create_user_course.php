<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_115537_create_user_course extends Migration
{
    protected $tableName = '{{%user_course}}';
    protected $tableUser = '{{%user}}';
    protected $tableCourse = '{{%course}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'user_id' => $this->integer()->notNull(),
            'course_id' => $this->integer()->notNull(),
        ], $tableOptions);


        $this->createIndex('idx-user_course-user_id', $this->tableName, 'user_id');
        $this->createIndex('idx-user_course-course_id', $this->tableName, 'course_id');

        $this->addForeignKey('fk-user_course-user-user_id',
            $this->tableName,
            'user_id',
            $this->tableUser,
            'id',
            'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk-user_course-course-course_id',
            $this->tableName,
            'course_id',
            $this->tableCourse,
            'id',
            'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropForeignKey('fk-user_course-user-user_id', $this->tableName);
        $this->dropForeignKey('fk-user_course-course-course_id', $this->tableName);

        $this->dropIndex('idx-user_course-user_id', $this->tableName);
        $this->dropIndex('idx-user_course-course_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
