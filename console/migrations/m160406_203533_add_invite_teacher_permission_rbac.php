<?php

use yii\db\Migration;

class m160406_203533_add_invite_teacher_permission_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;
        // add "inviteTeacher" permission
        $inviteTeacher = $auth->createPermission('inviteTeacher');
        $inviteTeacher->description = 'Invite Teacher';
        $auth->add($inviteTeacher);

        // get "admin" role and give this role additional permission
        // 'inviteTeacher'
        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $inviteTeacher);


    }
    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
