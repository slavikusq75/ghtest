<?php

use yii\db\Migration;

class m160415_003447_remove_some_teacher_permissions_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;
        $teacher = $auth->getRole('teacher');
        $admin = $auth->getRole('admin');
        $createCourse = $auth->getPermission('createCourse');

        $auth->removeChild($teacher, $createCourse);
        $auth->addChild($admin, $createCourse);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $teacher = $auth->getRole('teacher');
        $admin = $auth->getRole('admin');
        $createCourse = $auth->getPermission('createCourse');

        $auth->removeChild($admin, $createCourse);
        $auth->addChild($teacher, $createCourse);
    }
}
