<?php

use yii\db\Migration;

class m160411_211616_alter_username_email_phone_in_user extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'username', $this->string()->unique());
        $this->alterColumn('{{%user}}', 'email', $this->string()->unique());
        $this->alterColumn('{{%user}}', 'phone', $this->string()->unique());
    }

    public function down()
    {
        $this->alterColumn('{{%user}}', 'username', $this->string()->unique());
        $this->alterColumn('{{%user}}', 'email', $this->string()->unique());
        $this->alterColumn('{{%user}}', 'phone', $this->string()->unique());
    }
}